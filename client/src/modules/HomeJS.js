import React, { Component, useEffect, useReducer, useState } from "react";
import { Button, Table, Form } from 'react-bootstrap';

const HomeJS = props => {
   
    const [tbody, setTbody] = useState([]);
    useEffect(() => {
      setTbody(props.body);
    }, []);
  const subHeading = [];
  const heading = []
  tbody.forEach( e=>{
     for(var key in e){
     if(key !== 'Segment' && key !== 'Year' && key !== 'comments' && key !== 'targets'){
        heading.push(key);
     }
     else if(key === 'Segment'){
        e.Segment.forEach( seg =>{
          for(var key in seg){
              heading.push(key);
          }
        })
     }
    }
  })
   heading.forEach( e=>{
    if(e != 'Category' && e != 'Metric_Name' && e != 'Leading_Lagging' && e != 'UOM' ){
          subHeading.push('CYPT');
          subHeading.push('PYPT');
          subHeading.push('PY4PT');
    }
   }) 
   
   tbody.forEach( e=>{
    console.log(e);
    for(var key in e){

    if(key === 'Segment'){
      const segment = []
      console.log(key);
       e.Segment.forEach( seg =>{
         for(var key in seg){
           console.log("Key:" +key + "value: ",seg[key])
             segment.push(seg[key]);
             
         }
       })
       console.log(segment);
       e.targets = segment;
    }
   }
 })
 console.log(tbody)

  return (
    <Table responsive>
      <thead>
        <tr>
        <th rowSpan="2">#</th>
          {heading.map((v) => {
            
            if(v == 'Category' || v == 'Metric_Name' || v == 'Leading_Lagging' || v == 'UOM' ){
              return (<th rowSpan="2">{v}</th>)
            }else{
              return (<th colSpan="3">{v}</th>)
            }
            
          })}

        </tr>
        <tr>
         {subHeading.map((sub) => {  
          return ( <td> {sub} </td> )
         })} 
            
        </tr>
      </thead>
      <tbody>
        {tbody.map((item) => {
          return (
            <tr>
              <td><input type="checkbox"></input></td>
              <td>{item.Metric_Name}</td>
                  <td>{item.Category}</td>
                  <td>{item.UOM}</td>
                  <td>{item.Leading_Lagging}</td>
                  {
                    item.targets.map(seg =>{
                       return (<td colSpan= "3">
                         <td>{seg.Current_Year_Performance_Target}</td>
                      <td>{seg.Previous_Year_Performance_Target}</td>
                       <td>{seg.Previous_Year_Q4_Performance_Target}</td></td>)
                    })
                  } 
            </tr>
          )
        })} 
                
      </tbody>
    </Table>

    

  );
}

export default HomeJS;