import React, { Component, useState } from "react";
// import logo from '../mars_logo.png';
import logo from '../images/marsLogo.png';
import createUser from '../images/user.svg';
import useOutsideClick from "./useOutsideClick";
import { withStyles } from '@material-ui/styles';
import { makeStyles } from '@material-ui/core/styles';
import { useRef } from 'react';
import {
  BrowserRouter,
  useRouteMatch,
  Switch,
  Route,
  Link,
  useHistory, useLocation
} from 'react-router-dom';
import Common from '../utils/Common';
import PersonIcon from '@mui/icons-material/Person';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import CreateUser from "./persona/CreateUser";
import AuthProvider from "../services/AuthProvider";
// import { Tooltip } from "react-bootstrap";
import Tooltip from '@material-ui/core/Tooltip';


const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#484848',
    color: 'white',
    // boxShadow: theme.shadows[1],
    fontSize: 12,
  },
}))(Tooltip);


const useStyles = makeStyles(() => ({
  logoutIcon: {
    color: 'white',
    transition: 'transform 0.2s',
    "&:hover": {
      transform: 'scale(1.15)',
    }
  },
  profileIcon: {
    color: 'white',
    transition: 'transform 0.2s',
    "&:hover": {
      transform: 'scale(1.15)',
    }
  }
}))

function Header(props) {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();
  const [showCreateUser, setShowCreateUser] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const path = location.pathname;
  const ref = useRef();
  const commonService = new Common();
  // const signOut = new AuthProvider();
  console.log(path);
  //const [heading, setHeading ] = useState('');

  commonService.isAdmin().then(role => {
    if (role === "true") {
      setIsAdmin(true);
    }
  })
  let heading = '';
  if (path.includes("metricList")) {
    heading = "Corporate Metrics & Annual Targets"
    //setHeading("Corporate Metrics & Annual Targets")
  } else if (path.includes("rr_matrix")) {
    heading = "R&R Matrix"
    //setHeading("R&R Matrix")
  } else if (path.includes("metricInput")) {
    heading = "Metrics + Insight Input"
    //setHeading("Metrics + Insight Input")
  } else if (path.includes("metricView")) {
    heading = "Metrics + Insights View"
    //setHeading("Metrics + Insight View")
  } else if (path.includes("reportingTimeline")) {
    heading = "Reporting Timelines"
    //setHeading("Reporting Timelines")
  } else if (path.includes("reportingLayout")) {
    heading = "Reporting Layout"
    //setHeading("Reporting Layout")
  } if (path.includes("mainMenu")) {
    heading = "Main Menu"
    //setHeading("Corporate Metrics & Annual Targets")
  } if (path.includes("createUser")) {
    heading = "Create User"
    //setHeading("Corporate Metrics & Annual Targets")
  } if (path.includes("cdc")) {
    heading = "CDC Page"
    //setHeading("Corporate Metrics & Annual Targets")
  }
  const logoHeader = {
    width: "10%",
    padding: "0px",
    background: "#0000a0",
    border: "1px solid #0000a0"
  };
  const header = {
    width: "85%",
    padding: "0",
    background: "#0000a0",
    textAlign: "center",
    border: "1px solid #0000a0",
    // fontFamily: 'MarsCentra-Bold'
  };
  const logout = () => {
    props.onSignOut();
    commonService.removeUserSession();
    history.push("/login");
  }

  const goToPage = (value, page) => {
    createUserPopup();
    if (page === "createUser") {
      history.push("/createUser")
    } if (page === "viewUser") {
      history.push("/viewUser");
    }

  }
  const createUserPopup = () => {
    if (showCreateUser === true) {
      setShowCreateUser(false);
    } else {
      setShowCreateUser(true);
    }
  }

  const handleToggleClick = () => {
    setShowCreateUser(!showCreateUser);
  }

  useOutsideClick(ref, () => {
    if (showCreateUser) setShowCreateUser(false);
  });

  return (
    <div>
      <table>
        <tbody>
          <tr>
            <td style={logoHeader}>
              <Link to="/mainMenu"> <img src={logo} style={{ width: "100%" }}></img>
              </Link>
            </td>
            <td style={header}> <h1 style={{ color: "white", margin: "0" }}>{heading}</h1> </td>
            <td onClick={handleToggleClick} style={{ background: "rgb(0, 0, 160)", border: "1px solid rgb(0, 0, 160)", cursor: "pointer", width: "5%" }}>
              {path !== '/' && path !== '/login' && isAdmin === true && <div ref={ref} class="dropdown">
                <LightTooltip title="User" >
                  <PersonIcon onClick={createUserPopup} className={classes.profileIcon} style={{ fontSize: "39px" }} />
                </LightTooltip>
                {showCreateUser === true && <div id="myDropdown" class="dropdown-content">
                  <ul>
                    <li onClick={(value) => goToPage(value, "createUser")}>Create User</li>
                    <li onClick={(value) => goToPage(value, "viewUser")}>View User</li>
                  </ul>
                </div>}
              </div>}
            </td>
            {path !== '/' && path !== '/login' && <td style={{ padding: '5px 15px 5px 0px', background: "#0000a0", border: "1px solid #0000a0", color: "white", cursor: "pointer", margin: "0" }}>
              {/* <h2 onClick={(value) => logout()} >Logout</h2> */}
              <LightTooltip title="Logout" >
                <ExitToAppIcon className={classes.logoutIcon} onClick={(value) => logout()} style={{ fontSize: "32px" }} />
              </LightTooltip>
            </td>
            }
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Header;