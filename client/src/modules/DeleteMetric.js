import React ,  {useState} from "react";
import './Popup.css';
import HomeService from "../services/HomeService";
const DeleteMetric = props => {
     const [metric, setMetric ] = useState(props.metric);
     const [metricToDelete, setMetricToDelete] = useState({"metric_id":[metric.ID], "comment":""});
     //const [loading, setloading] = useState(false);
     const homeService = new HomeService();
     //console.log(metricToDelete)
    const change = (value) => {
        metricToDelete.comment = value.target.value
    }
    const handleDelete = (value)=>{
        props.deleteMetrix(metricToDelete);
        // setLoading(!loading);
        // homeService.deleteMetrix(metricToDelete).then(item => {
        //     if (item?.status === 'Success') {
        //       setLoading(!loading);
        //       var tbody = [...this.state.body];
        //       this.state.metrixToBeDeleted.forEach(e => {
        //         var index = tbody.findIndex(m => m.ID === e);
        //         if (index > -1) {
        //           tbody.splice(index, 1);
        //         }
        //       })
        //       this.setState({ ...this.state, body: tbody })
        //       this.processTable(tbody);
        //     }else{
        //         setLoading(!loading);
        //     }
        //   });
    }
  return (
    <div className="popup-box">
      <div className="box">
          <div style={{padding: "15px", backgroundColor : "#8080cf"}}>
             <span className="close-icon" onClick={props.handleClose}>x</span>
             <h3 style={{margin: "0"}}>Delete Metric</h3>
          </div>
        <div className="modal-box">
        <label className="input-label">
              Metric To Be Deleted
              <input type="text" name="metricName" disabled value={metric["Metric Name"]} />
              {/* <select onChange={(value)=> change(value,'Segment')}>
                <option value="Select">Select Segment</option>
                {
                  segment.map( e =>{
                    return (
                      <option key={e} value={e}>{e}</option>
                  )
                  })
                  
                }
              </select> */}
              {/* <input type="text" name="Category" /> */}
            </label>
            <label className="input-label">
              Comment
              {/* <input type="text" name="comment"/>  */}
              <input type="text" name="comment" onChange={(value)=> change(value)} />
            </label>
            <div style={{textAlign: "center", margin: "5%"}}>
            <button onClick={(value)=> handleDelete(value)}>Delete</button>
            <button onClick={props.handleClose}>Cancel</button>
            </div>
            
          </div>
      </div>
    </div>
  );
};
 
export default DeleteMetric;