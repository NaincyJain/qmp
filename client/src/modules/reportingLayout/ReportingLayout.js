import React from "react";
import { Table } from "react-bootstrap";
import HomeService from "../../services/HomeService";
import './reporting.css';
import pptxgen from "pptxgenjs";

class ReportingLayout extends React.Component {

    constructor() {
        super();
        this.state = {
            reportToShow: "Detailed",
            selectedYear: "",
            selectedQuater: "",
            selectedOverviewYear: "",
            selectedOverviewQuater: "",
            headings: [],
            noOfSegmentColumn: 0,
            selectedMetricType: "Product",
            reportingData: [],
            tableData: [],
            year: [],
            loading: false,
            overViewReportData: [],
            productData: [],
            serviceData: [],
            productHeading: [],
            serviceHeading: [],
            overviewProductSegmentNo: 0,
            overviewServiceSegmentNo: 0,
            popUpModelOpen: false
        }
        this.showReport = this.showReport.bind(this);
        this.homeService = new HomeService();
        this.selectYear = this.selectYear.bind(this);
        this.downloadCSV = this.downloadCSV.bind(this);
        this.exportTableToCSV = this.exportTableToCSV.bind(this);
        this.downloadPPt = this.downloadPPt.bind(this);
        this.setLoading = this.setLoading.bind(this);
        this.exportTableToCSVOverViewReport = this.exportTableToCSVOverViewReport.bind(this);
    }
    componentDidMount() {
        this.getDetailedReport();
        this.getOverviewReport();
        this.getYearList();
    }
    setLoading() {
        this.setState({ loading: !this.state.loading, popUpModelOpen: !this.state.popUpModelOpen });
    }
    selectYear(event) {
        this.setState({ selectedYear: event.target.value });
    }
    getYearList() {
        this.homeService.getYearFilter().then(item => {
            if (item.status == 'Success') {
                let data = JSON.parse(item.data);
                this.setState({ ...this.state, year: data });
            }
        })
    }
    filterDetailReport(filter, event) {
        var obj = {};
        if (filter === "Type") {
            this.setState({ selectedMetricType: event.target.value })
            obj = {
                "Year": this.state.selectedYear,
                "Quarter": this.state.selectedQuater,
                "type": event.target.value
            }
        } else if (filter === "Year") {
            this.setState({ selectedYear: event.target.value })
            obj = {
                "Year": event.target.value,
                "Quarter": this.state.selectedQuater,
                "type": this.state.selectedMetricType
            }
        } else {
            this.setState({ selectedQuater: event.target.value })
            obj = {
                "Year": this.state.selectedYear,
                "Quarter": event.target.value,
                "type": this.state.selectedMetricType
            }
        }
        this.setLoading();
        this.homeService.displayDetailedReport(obj).then(item => {
            this.setLoading();
            this.setState({ reportingData: JSON.parse(item.data) })
            this.processData(JSON.parse(item.data), "DetailReport");
        })
    }
    filterOverviewReport(filter, event) {
        var obj = {};
        if (filter === "Year") {
            this.setState({ selectedOverviewYear: event.target.value })
            obj = {
                "Year": event.target.value,
                "Quarter": this.state.selectedOverviewQuater,
            }
        } else {
            this.setState({ selectedOverviewQuater: event.target.value })
            obj = {
                "Year": this.state.selectedOverviewYear,
                "Quarter": event.target.value,
            }
        }
        this.setLoading();
        this.homeService.displayOverviewReport(obj).then(item => {
            //     this.setLoading();
            this.setLoading();
            console.log("Overview report");
            console.log(JSON.parse(item.data.Product));
            console.log(JSON.parse(item.data.Service));
            var overViewData = {
                Product: JSON.parse(item.data.Product),
                Service: JSON.parse(item.data.Service)
            }
            this.setState({ overViewReportData: overViewData })
            this.processData(overViewData.Product, "ProductReport");
            this.processData(overViewData.Service, "ServiceReport");
        })
    }
    getDetailedReport() {
        var today = new Date();
        var quarter = Math.floor((today.getMonth() + 3) / 3);
        var obj = {
            "Year": today.getFullYear(),
            "Quarter": "Q" + quarter,
            "type": this.state.selectedMetricType
        }
        this.setLoading();
        this.setState({ selectedYear: today.getFullYear(), selectedQuater: "Q" + quarter })
        this.homeService.displayDetailedReport(obj).then(item => {
            this.setLoading();
            this.setState({ reportingData: JSON.parse(item.data) })
            this.processData(JSON.parse(item.data), "DetailReport");
        })
    }
    getOverviewReport() {

        var today = new Date();
        var quarter = Math.floor((today.getMonth() + 3) / 3);
        var obj = {
            "Year": today.getFullYear(),
            "Quarter": "Q" + quarter
        }
        // this.setLoading();
        this.homeService.displayOverviewReport(obj).then(item => {
            //     this.setLoading();
            console.log("Overview report");
            console.log(JSON.parse(item.data.Product));
            console.log(JSON.parse(item.data.Service));
            var overViewData = {
                Product: JSON.parse(item.data.Product),
                Service: JSON.parse(item.data.Service)
            }
            this.setState({ overViewReportData: overViewData, selectedOverviewYear: today.getFullYear(), selectedOverviewQuater: "Q" + quarter })
            this.processData(overViewData.Product, "ProductReport");
            this.processData(overViewData.Service, "ServiceReport");
        })
    }
    processData(item, reportType) {
        var tableData = [...item];
        tableData.forEach(e => {
            var segment = {};
            for (var key in e) {
                if (key.includes('Insights') || key.includes('Performance') || key.includes('Performance_Target') || key.includes('Previous_Year_Q4_Performance')) {
                    segment[key] = e[key];
                    delete e[key];
                }
            }
            e["seg"] = segment;
        })
        tableData.forEach(e => {
            var arr = []
            var insigth = null;
            for (var key in e.seg) {

                var target = key.split('_')
                var targetKey = target[0].trim();
                var obj = {}
                var temp = {};
                //var bms = [];

                for (var key_temp in e.seg) {
                    if (key_temp.includes(target[0])) {
                        if (key_temp.includes('Previous_Year_Q4_Performance')) {
                            temp['Previous_Year_Q4_Performance'] = e.seg[key_temp];
                        } else if (key_temp.includes('Performance_Target')) {
                            temp['Performance_Target'] = e.seg[key_temp];
                        } else if (key_temp.includes('Insights')) {
                            temp['Insights'] = e.seg[key_temp];
                            if (insigth === null && e.seg[key_temp] != null) {
                                insigth = e.seg[key_temp];
                            } else if (insigth != null && e.seg[key_temp] != null) {
                                insigth = insigth + "," + e.seg[key_temp];
                            }

                        } else if (key_temp.includes('Performance')) {
                            if (e.seg[key_temp] != null) {
                                var perf = e.seg[key_temp].split(";");
                                temp['Performance'] = perf[0];
                                temp['target'] = perf[1];
                            } else {
                                temp['Performance'] = null;
                                temp['target'] = null;
                            }


                        }
                        delete e.seg[key_temp];
                    }
                }
                obj[targetKey] = temp;
                arr.push(obj);
                delete e.seg[key];
            }
            e["Segment"] = arr;
            e["insigth"] = insigth;
            delete e["seg"];
        })
        tableData.forEach(e => {
            for (var key in e) {
                if (key === 'Segment') {
                    const segment = []
                    e.Segment.forEach(seg => {
                        for (var key in seg) {
                            segment.push(seg[key]);
                        }
                    })
                    e["targets"] = segment;
                    e["targetTemp"] = [...segment];
                }
            }
        })
        var heading = [];
        for (var key in tableData[0]) {
            if (key !== 'Segment' && key !== 'ID' && key !== 'Year' && key !== 'targets' && key !== 'targetTemp' && key != 'insigth' &&
                !key.includes("PYQ4_Perf")) {
                heading.push(key);
            }
            else if (key === 'Segment') {
                tableData[0].Segment.forEach(seg => {
                    for (var key in seg) {
                        heading.push(key);
                    }
                })
            }
        }
        var segmentNumber = tableData[0]?.Segment?.length * 3;
        tableData.sort((a, b) => (a.Category > b.Category) ? 1 : ((b.Category > a.Category) ? -1 : 0))
        if (reportType === "DetailReport") {
            this.setState({ tableData: tableData, headings: heading, noOfSegmentColumn: segmentNumber });
        } else if (reportType === "ProductReport") {
            this.setState({ productData: tableData, productHeading: heading, overviewProductSegmentNo: segmentNumber });
        } else {
            this.setState({ serviceData: tableData, serviceHeading: heading, overviewServiceSegmentNo: segmentNumber });
        }

        this.getColorCoding(reportType);
    }
    getColorCoding(reportType) {
        var data = [];
        if (reportType === "DetailReport") {
            data = [...this.state.tableData];
        } else if (reportType === "ProductReport") {
            data = [...this.state.productData];
        } else {
            data = [...this.state.serviceData];
        }
        data.forEach(e => {
            e.targetTemp.forEach(target => {
                var performanceTarget = parseInt(target.Performance_Target);
                var previousYearPerformance = parseInt(target.Previous_Year_Q4_Performance)
                var performance = parseInt(target.Performance);
                if (performance === 0) {
                    target["color"] = "No target";
                } else if (isNaN(performance)) {
                    target["color"] = "No data";
                } else if (performance >= performanceTarget) {
                    target["color"] = "green";
                } else if (performance < previousYearPerformance) {
                    target["color"] = "red";
                } else if (performance >= previousYearPerformance) {
                    target["color"] = "yellow";
                }
            })
        })
        if (reportType === "DetailReport") {
            this.setState({ ...this.state, tableData: data });
        } else if (reportType === "ProductReport") {
            this.setState({ ...this.state, productData: data });
        } else {
            this.setState({ ...this.state, serviceData: data });
        }
        this.mergeSameCategoryColumn(data, reportType);
    }
    mergeSameCategoryColumn(data, reportType) {
        var tableData = [...data];
        for (var i = 0; i < tableData.length; i++) {
            var count = 0;
            for (var j = i + 1; j < tableData.length; j++) {
                if (tableData[i].Category === tableData[j].Category) {
                    count++;
                    delete tableData[j]["Category"];
                    //delete tableData[i]["Category"];
                }
            }
            tableData[i]["categoryCount"] = count + 1;
        }
        console.log("mergeSameCategoryColumn");
        console.log(tableData);
        if (reportType === "DetailReport") {
            this.setState({ ...this.state, tableData: data });
        } else if (reportType === "ProductReport") {
            this.setState({ ...this.state, productData: data });
        } else {
            this.setState({ ...this.state, serviceData: data });
        }

    }
    showReport(event) {
        this.setState({ reportToShow: event.target.value });
    }
    downloadCSV(csv, filename) {
        var csvFile;
        var downloadLink;

        // CSV file
        csvFile = new Blob([csv], { type: "text/xls" });

        // Download link
        downloadLink = document.createElement("a");

        // File name
        downloadLink.download = filename;

        // Create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);

        // Hide download link
        downloadLink.style.display = "none";

        // Add the link to DOM
        document.body.appendChild(downloadLink);

        // Click download link
        downloadLink.click();
    }
    exportTableToCSV() {
        var html, link, blob, url, css;

        // EU A4 use: size: 841.95pt 595.35pt;
        // US Letter use: size:11.0in 8.5in;

        css = (
            '<style>' +
            'table{border-collapse:collapse;font-family: Arial;}td{width: 5%;padding: 5px 5px 5px 5px;font-size: 8px;}' +
            'thead tr { font-size: 8px;}' +
            '#detailReporttbl th {text-align: center; background-color: #f4f1f1;color: #00d7b9;border: 1px solid white;font-weight: 600;}' +
            '#detailReporttbl td {background-color: #f4f1f1;color: black;border: 1px solid white;}' +
            '</style>'
        );

        html = window.docx.innerHTML;
        blob = new Blob(['\ufeff', css + html], {
            type: 'application/msword'
        });
        url = URL.createObjectURL(blob);
        link = document.createElement('A');
        link.href = url;
        // Set default file name. 
        // Word will append file extension - do not add an extension here.
        link.download = 'Document';
        document.body.appendChild(link);
        if (navigator.msSaveOrOpenBlob) navigator.msSaveOrOpenBlob(blob, 'Document.doc'); // IE10-11
        else link.click();  // other browsers
        document.body.removeChild(link);
    }

    exportTableToCSVOverViewReport() {
        var htmlProduct, htmlService, link, blob, url, css;

        // EU A4 use: size: 841.95pt 595.35pt;
        // US Letter use: size:11.0in 8.5in;

        css = (
            '<style>' +
            'table{border-collapse:collapse;font-family: Arial;}td{width: 5%;padding: 5px 5px 5px 5px;font-size: 8px;}' +
            'thead tr { font-size: 8px;}' +
            '#overviewProductReporttbl th {text-align: center; background-color: #0000a0;color: #fff;border: 1px solid white;font-weight: 600;}' +
            '#overviewProductReporttbl td {background-color: #f4f1f1;color: black;border: 1px solid white;}' +
            '#overviewServiceReporttbl th {text-align: center; background-color: #0000a0;color: #fff;border: 1px solid white;font-weight: 600;}' +
            '#overviewServiceReporttbl td {background-color: #f4f1f1;color: black;border: 1px solid white;}' +
            '</style>'
        );

        htmlProduct = window.overviewProductDocx.innerHTML;
        htmlService = window.overviewServiceDocx.innerHTML;
        blob = new Blob(['\ufeff', css + htmlProduct + "<br><br><br>" + htmlService], {
            type: 'application/msword'
        });
        url = URL.createObjectURL(blob);
        link = document.createElement('A');
        link.href = url;
        // Set default file name. 
        // Word will append file extension - do not add an extension here.
        link.download = 'Document';
        document.body.appendChild(link);
        if (navigator.msSaveOrOpenBlob) navigator.msSaveOrOpenBlob(blob, 'Document.doc'); // IE10-11
        else link.click();  // other browsers
        document.body.removeChild(link);
    }
    downloadOverviewreportToppt(){
        let pptx = new pptxgen();
        const options ={ valign:'top',autoFit:true,fontFace:'Arial',addHeaderToEach:true};
        pptx.defineLayout({ name:'A3', width:16, height:9 });
        pptx.layout='A3'
        pptx.tableToSlides("overviewProductReporttbl", {autoFit:true,fontFace:'Arial',addHeaderToEach:true});
        pptx.tableToSlides("overviewServiceReporttbl", {autoFit:true,fontFace:'Arial',addHeaderToEach:true});
        pptx.writeFile({ fileName: "html2pptx-demo.pptx" });
    }
    downloadPPt() {
        let pptx = new pptxgen();
        const options ={ valign:'top',autoFit:true,fontFace:'Arial',addHeaderToEach:true};
        pptx.defineLayout({ name:'A3', width:16, height:12 });
        pptx.layout='A3'
        pptx.tableToSlides("detailReporttbl", {autoFit:true,fontFace:'Arial',addHeaderToEach:true});
        // pptx.tableToSlides("detailReporttbl", {autoFit:true,fontFace:'Arial',addHeaderToEach:true});
        pptx.writeFile({ fileName: "html2pptx-demo.pptx" });
        // 1. Create a new Presentation
        //let pres = new pptxgen();

        // 2. Add a Slide
        //let slide = pres.addSlide();

        // TABLE 1: Single-row table
        // let rows = [["Cell 1", "Cell 2", "Cell 3"]];
        // slide.addTable(rows, { w: 9 });

        // TABLE 2: Multi-row table
        // - each row's array element is an array of cells
        // let rows = [["A1", "B1", "C1"]];
        // slide.addTable(rows, { align: "left", fontFace: "Arial" });

        // TABLE 3: Formatting at a cell level
        // - use this to selectively override the table's cell options
        // let rows = [
        //     [
        //         { text: "Top Lft", options: { align: "left", fontFace: "Arial" } },
        //         { text: "Top Ctr", options: { align: "center", fontFace: "Verdana" } },
        //         { text: "Top Rgt", options: { align: "right", fontFace: "Courier" } },
        //     ],
        // ];
        // slide.addTable(rows, { w: 9, rowH: 1, align: "left", fontFace: "Arial" });

        // 4. Save the Presentation
        //pres.writeFile();
    }
    render() {
        return (
            <div>
                {this.state.loading && <div className="loader-box">
                    <div id="loader"></div>
                </div>}
                <label className="input-label" style={{ padding: "0", textAlign: "center" }}>
                    <div>
                        <input style={{margin: "3px 3px"}} type="radio" defaultChecked id="productReport" name="report" value="Detailed"
                            onChange={this.showReport} />
                        <label style={{verticalAlign: "top"}} >Detailed Report</label>
                        {/*   <input type="radio" id="serviceReport" name="report" value="Detailed_Service_Report" />
              <label>Detailed Service Report</label> */}
                        <input style={{margin: "3px 3px"}} type="radio" id="overviewReport" name="report" value="Overview"
                            onChange={this.showReport} />
                        <label  style={{verticalAlign: "top"}}>Overview Report</label>
                    </div>
                </label>
                {this.state.reportToShow === "Detailed" &&
                    <div>
                        <div className="filter">
                            <div className="filterDiv">
                                <label>Report Type</label>
                                <select className="filterDropDown" onChange={this.filterDetailReport.bind(this, "Type")} value={this.state.selectedMetricType}>
                                    <option value="Product">Product</option>
                                    <option value="Service">Service</option>
                                </select>
                            </div>
                            <div className="filterDiv">
                                <label>Year</label>
                                <select className="filterDropDown" onChange={this.filterDetailReport.bind(this, "Year")} value={this.state.selectedYear}>
                                    {
                                        this.state.year.map((y) => {
                                            return (<option key={y} value={y}>{y}</option>)
                                        })
                                    }
                                </select>
                            </div>
                            <div className="filterDiv">
                                <label>Quarter</label>
                                <select className="filterDropDown" onChange={this.filterDetailReport.bind(this, "Quater")} value={this.state.selectedQuater}>
                                    <option value="Q1">Q1</option>
                                    <option value="Q2">Q2</option>
                                    <option value="Q3">Q3</option>
                                    <option value="Q4">Q4</option>
                                </select>
                            </div>
                            <button onClick={this.exportTableToCSV}>Download</button>
                            <button onClick={this.downloadPPt}>Download PPT</button>
                        </div>
                        <div style={{ overflow: "auto", height: "67vh" }} id="docx">
                            <table responsive style={{ width: "100%" }} id="detailReporttbl">
                                <thead>
                                    <tr>
                                        <th style={{ ...(this.state.popUpModelOpen ? { position: "inherit", background: "white" } : { background: "white" }) }} colSpan="4"></th>
                                        <th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} colSpan={this.state.noOfSegmentColumn}>{this.state.selectedYear} {this.state.selectedQuater}</th>
                                        <th  style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} rowSpan="2">INSIGHTS (Key successes/challenges/opportunities)</th>
                                    </tr>
                                    <tr>
                                        {
                                            this.state.headings.map(head => {
                                                if (head === 'Category' || head === 'Metric Name' || head === 'Leading\/Lagging' || head === 'UOM') {
                                                    if (head === 'Category') {
                                                        return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit", background: "white", color: "white" } : { background: "white", color: "white" }) }}>{head}</th>)
                                                    } else if (head === 'Leading\/Lagging') {
                                                        return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} >Lead or Lag</th>)
                                                    } else {
                                                        return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} >{head}</th>)
                                                    }

                                                } else {
                                                    return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} colSpan="3">{head}</th>)
                                                }
                                            })
                                        }
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.tableData.map(data => {
                                            return (<tr>
                                                {data.Category && <td rowSpan={data.categoryCount} style={{ backgroundColor: "rgb(91 91 184)", color: "white" }}>{data.Category}</td>}
                                                <td>{data["Metric Name"]}</td>
                                                <td>{data["Leading/Lagging"]}</td>
                                                <td>{data.UOM}</td>
                                                {

                                                    data.targetTemp.map(seg => {
                                                        return (<td
                                                            style={{
                                                                ...(seg["color"] === "yellow" ? { background: "#ffdc00",textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "red" ? { background: "#ff3c14" ,textAlign:"center"} : {textAlign:"center"}),
                                                                ...(seg["color"] === "green" ? { background: "#a6db00",textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "No target" ? { background: "#f4f1f1",textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "No data" ? { background: "#aba9a9",textAlign:"center" } : {textAlign:"center"})
                                                            }}
                                                            colSpan="3">{seg.Performance} <br/> ({seg.target})
                                                        </td>)
                                                        // seg.bsm.map(b =>{
                                                        //     return(<td>{seg}</td>)
                                                        // })
                                                        // if(seg.bsm.length === 0){
                                                        // return (<td colSpan="3">{seg.Performance}</td>)
                                                        //     }
                                                    })
                                                }
                                                <td>{data.insigth}</td>
                                            </tr>)
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                }

                {
                    this.state.reportToShow === "Overview" &&

                    <div>
                        <div className="filter">
                            <div className="filterDiv">
                                <label>Year</label>
                                <select className="filterDropDown" onChange={this.filterOverviewReport.bind(this, "Year")} value={this.state.selectedOverviewYear}>
                                    {
                                        this.state.year.map((y) => {
                                            return (<option key={y} value={y}>{y}</option>)
                                        })
                                    }
                                </select>
                            </div>
                            <div className="filterDiv">
                                <label>Quarter</label>
                                <select className="filterDropDown" onChange={this.filterOverviewReport.bind(this, "Quater")} value={this.state.selectedOverviewQuater}>
                                    <option value="Q1">Q1</option>
                                    <option value="Q2">Q2</option>
                                    <option value="Q3">Q3</option>
                                    <option value="Q4">Q4</option>
                                </select>
                            </div>
                            <button onClick={this.exportTableToCSVOverViewReport}>Download</button>
                            <button onClick={this.downloadOverviewreportToppt}>Download ppt</button>
                        </div>
                        <div style={{ overflow: "auto", height: "67vh", width: "50%", float: "left" }} id="overviewProductDocx">
                            <table responsive style={{ width: "100%" }} id="overviewProductReporttbl">
                                <thead>
                                    <tr>
                                        <th style={{ ...(this.state.popUpModelOpen ? { position: "inherit", background: "white" } : { background: "white" }) }}></th>
                                        <th style={{ ...(this.state.popUpModelOpen ? { position: "inherit", background: "#00d7b9" } : { background: "#00d7b9" }) }} colSpan="2" >Product</th>
                                        <th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} colSpan={this.state.overviewProductSegmentNo}>{this.state.selectedOverviewYear} {this.state.selectedOverviewQuater}</th>
                                    </tr>
                                    <tr>
                                        {
                                            this.state.productHeading.map(head => {
                                                if (head !== 'Leading\/Lagging' && head !== 'UOM') {
                                                    if (head === 'Category' || head === 'Metric Name') {
                                                        if (head === 'Category') {
                                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit", background: "white", color: "white" } : { background: "white", color: "white" }) }} >{head}</th>)
                                                        } else {
                                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} colSpan="2" >{head}</th>)
                                                        }

                                                    } else {
                                                        return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} colSpan="3">{head}</th>)
                                                    }
                                                }

                                            })
                                        }
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.productData.map(data => {
                                            return (<tr>
                                                {data.Category && <td rowSpan={data.categoryCount}>{data.Category}</td>}
                                                <td>{data["Metric Name"]}</td>
                                                <td></td>
                                                {

                                                    data.targetTemp.map(seg => {
                                                        return (<td
                                                            style={{
                                                                ...(seg["color"] === "yellow" ? { background: "#ffdc00", textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "red" ? { background: "#ff3c14", textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "green" ? { background: "#a6db00" ,textAlign:"center"} : {textAlign:"center"}),
                                                                ...(seg["color"] === "No target" ? { background: "#f4f1f1",textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "No data" ? { background: "#aba9a9" ,textAlign:"center"} : {textAlign:"center"})
                                                            }}
                                                            colSpan="3">{seg.Performance} <br/>({seg.target})</td>)
                                                    })
                                                }
                                            </tr>)
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                        <div style={{ overflow: "auto", height: "67vh", width: "48%", float: "right" }} id="overviewServiceDocx">
                            <table responsive style={{ width: "100%" }} id="overviewServiceReporttbl">
                                <thead>
                                    <tr>
                                        <th style={{ background: "white" }}></th>
                                        <th style={{ background: "#9600ff" }}>Service</th>
                                        <th colSpan={this.state.overviewServiceSegmentNo}>{this.state.selectedYear} {this.state.selectedQuater}</th>
                                    </tr>
                                    <tr>
                                        {
                                            this.state.serviceHeading.map(head => {
                                                if (head !== 'Leading\/Lagging' && head !== 'UOM') {
                                                    if (head === 'Category' || head === 'Metric Name') {
                                                        if (head === 'Category') {
                                                            return (<th style={{ background: "white", color: "white" }}>{head}</th>)
                                                        } else {
                                                            return (<th>{head}</th>)
                                                        }

                                                    } else {
                                                        return (<th colSpan="3">{head}</th>)
                                                    }
                                                }

                                            })
                                        }

                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.serviceData.map(data => {
                                            return (<tr>
                                                {data.Category && <td rowSpan={data.categoryCount}>{data.Category}</td>}
                                                <td>{data["Metric Name"]}</td>
                                                {

                                                    data.targetTemp.map(seg => {
                                                        return (<td
                                                            style={{
                                                                ...(seg["color"] === "yellow" ? { background: "#ff8200" ,textAlign:"center"} : {textAlign:"center"}),
                                                                ...(seg["color"] === "red" ? { background: "#ff3c14",textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "green" ? { background: "#a6db00",textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "No target" ? { background: "#f4f1f1",textAlign:"center" } : {textAlign:"center"}),
                                                                ...(seg["color"] === "No data" ? { background: "#aba9a9",textAlign:"center" } : {textAlign:"center"})
                                                            }}
                                                            colSpan="3">{seg.Performance} <br/> ({seg.target})</td>)
                                                    })
                                                }
                                            </tr>)
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default ReportingLayout;