import React, {useState, useEffect} from "react";
import  MultiSelect  from "react-multi-select-component";

const MetricNameDropDown = props => {
    const [metricName, setMetricName ] = useState(props.metricNameList);
    // const [metricName, setMetricName ] = useState([]);
    const [resetSelectedMetric, setResetSelectedMetric ] = useState(props.resetSelectedMetric);
    const [selectedMetricName, setSelectedMetricName] = useState([]);

    useEffect(() => {
        console.log("Use effect")
        setMetricName(props.metricNameList)
        // setMetricName([])
        setResetSelectedMetric(props.resetSelectedMetric)
        if(resetSelectedMetric === true){
            setSelectedMetricName([]);
        }
      }, [props.metricNameList,props.resetSelectedMetric]);

// console.log("resetSelectedMetric",props.resetSelectedMetric)
    const selectedMetric = (metricNameparams) =>{

console.log("selectedMetricName__",props.metricNameList,props.resetSelectedMetric,metricNameparams)
        
            console.log("MetricNameDropDown__")
            setSelectedMetricName(metricNameparams);
            props.filterMetricByName(metricNameparams);
    }

    return (
        <MultiSelect
                options={metricName}
                 value={selectedMetricName}
                onChange={(name) => selectedMetric(name)}
                style={{float: 'right'}}
                labelledBy="Select"
              />
    )

}

export default MetricNameDropDown;