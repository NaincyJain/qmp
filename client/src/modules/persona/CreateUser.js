import React, { useState, useEffect } from "react";
import MultiSelect from "react-multi-select-component";
import yearlyTarget from '../../images/metric_list_yearly_target.png';
import metrixInput from '../../images/metrix_insight_input.png';
import metrixView from '../../images/metrix_insight_view.png';
import reportingLayout from '../../images/reporting_layouts.png';
import reportingTimelines from '../../images/reporting_timelines.png';
import rrMatrix from '../../images/rr_matrix.png';
import home from '../../home.png';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import HomeService from "../../services/HomeService";
import {
    BrowserRouter,
    useRouteMatch,
    Switch,
    Route,
    Link,
    useHistory
} from "react-router-dom";
function CreateUser() {
    const history = useHistory();
    // const [persona, setPersona] = useState([
    //     { label: "Corporate Admin", value: 1 },
    //     { label: "Corporate System ADMIN", value: 2 },
    //     { label: "Corporate Reporting Team", value: 3 },
    //     { label: "Target Definition", value: 4 },
    //     { label: "Segment Data", value: 5 },
    //     { label: "Segment Insights", value: 6 },
    //     { label: "Corporate Data & Insights", value: 7 }
    // ]);
    const [persona, setPersona] = useState(["Corporate Admin", "Corporate System ADMIN", "Corporate Reporting Team", "Target Definition", "Segment Data",
        "Segment Insights", "Corporate Data", "Corporate Insights", "View-Only"])
    const [segments, setSegments] = useState([]);
    const [selectedPersona, setSelectedPersona] = useState([{ key: "", value: [] }])
    const [userData, setUserData] = useState({ "user_name": "", "user_mars_ad_name": "", "user_email": "", "user_password": "" });
    const homeService = new HomeService;
    const [loading, setloading] = useState(false);
    useEffect(() => {
        // Update the document title using the browser API
        console.log("Use Effect called")
        if (segments.length === 0) {
            homeService.getSegmentList().then(item => {
                var data = JSON.parse(item.data);
                var segment = [];
                console.log(data);
                data.forEach(element => {
                    segment.push({ label: element, value: element });
                });
                console.log(segment);
                setSegments(segment);
                setSelectedPersona([{ key: "View-Only", value: segment }])
            })
        }
    });

    const selectPersona = (event, persona) => {
        console.log(persona);
        persona.key = event.target.value;
        console.log(persona);
        var personaKey = [...selectedPersona]
        var index = personaKey.findIndex(e => e.key === event.target.value);
        personaKey.splice(index, 1, persona)
        setSelectedPersona(personaKey);
        console.log(selectedPersona);
    }
    const selectedSegment = (name, persona) => {
        console.log(persona);
        persona.value = name;
        // name.forEach(n =>{
        //     persona.value.push(n.value);
        // })
        console.log(persona);
        var personaValue = [...selectedPersona]
        var index = personaValue.findIndex(e => e.key === persona.key);
        personaValue.splice(index, 1, persona)
        setSelectedPersona(personaValue);
        console.log(selectedPersona);
        //setSelectedSegmentName(name);
    }
    const addPersona = (value) => {
        console.log("Add persona");
        console.log(selectedPersona);
        var personaValue = [...selectedPersona]
        personaValue.push({ key: "View-Only", value: segments });
        setSelectedPersona(personaValue);

    }
    const setUserInfo = (event, key) => {
        userData[key] = event.target.value;
        setUserData(userData);
        console.log(userData);
    }
    const saveUser = (value) => {
        var persona = [...selectedPersona];
        // var data = JSON.parse(JSON.stringify(userData));
        // var obj ={}
        setloading(true);
        persona.forEach(e => {
            var segments = [];
            e.value.forEach(seg => {
                segments.push(seg.value);
            })
            userData[e.key] = segments;

        })
        console.log("Data to be saved");
        console.log(userData);
        homeService.addNewUser(userData).then(item => {
            console.log(item);
            setloading(false);
            if(item.status === "Success"){
                //alert("User Created Successfully");
                window.open("/createUser","_self");
               // document.getElementById("createUser").clear();
            //    var inputType = document.querySelector('input');
            //    console.log(inputType)
            //   var obj =   { "user_name": "", "user_mars_ad_name": "", "user_email": "", "user_password": "" }
            //   setUserData(obj);
            //   setSelectedPersona([{ key: "View-Only", value: segments }])
            }else{
                alert("Unable to create user");
            }
            
        })
    }
    const deleteSelectedPersona = (value, index) => {
        var personaValue = [...selectedPersona]
        personaValue.splice(index, 1);
        setSelectedPersona(personaValue);
    }
    const optionImage = {
        width: "100%",
        // border: "1px solid",
        // padding: "10px",
        // borderRadius: "8px",
        cursor: "pointer",
        margin: "10px 0"
    }
    const personaTable = {
        border: "none",
        padding: "0",
        fontSize: "14px",
        width: "50%"
    }
    const segmentDropDown = {
        border: "none",
        padding: "0",
        fontSize: "14px",
        width: "45%"
    }
    const deletePersona = {
        border: "none",
        padding: "0px 10px",
        fontSize: "14px",
        cursor: "pointer"
    }
    return (
        <div>
            {loading && <div className="loader-box">
                <div id="loader"></div>
            </div>
            }
            <div style={{
                width: "3%",
                float: "left",
                padding: "5px 10px",
                height: "85vh",
                borderRight: "1px solid #e2dede"
            }}>
                <ul style={{ listStyle: "none", paddingInlineStart: "10px" }}>
                    <li onClick={() => history.push('/mainMenu')}>
                        <OverlayTrigger
                            key="mainMenu"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-metricList" className="my-tooltip">
                                    Home
                                </Tooltip>
                            }
                        >
                            <img src={home} style={optionImage} />
                        </OverlayTrigger>

                    </li>
                    <li onClick={() => history.push('/dashboard/metricList')}>
                        <OverlayTrigger
                            key="metricList"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-metricList" className="my-tooltip">
                                    Corporate Metrics & Annual Targets
                                </Tooltip>
                            }
                        >
                            <img src={yearlyTarget} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/rr_matrix')}>
                        <OverlayTrigger
                            key="rrMatrix"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-rrMatrix" className="my-tooltip">
                                    R&R Matrix
                                </Tooltip>
                            }
                        >
                            <img src={rrMatrix} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/metricInput')}>
                        <OverlayTrigger
                            key="metricInput"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-metricInput" className="my-tooltip">
                                    Metrics + Insight Input
                                </Tooltip>
                            }
                        >
                            <img src={metrixInput} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/metricView')}>
                        <OverlayTrigger
                            key="metricView"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-metricView" className="my-tooltip">
                                    Metrics + Insights View
                                </Tooltip>
                            }
                        >
                            <img src={metrixView} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/reportingTimeline')}>
                        <OverlayTrigger
                            key="reportingTimeline"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-reportingTimeline" className="my-tooltip">
                                    Reporting Timelines
                                </Tooltip>
                            }
                        >
                            <img src={reportingTimelines} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/reportingLayout')}>
                        <OverlayTrigger
                            key="reportingLayout"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-reportingLayout" className="my-tooltip">
                                    Reporting Layout
                                </Tooltip>
                            }
                        >
                            <img src={reportingLayout} style={optionImage} />
                        </OverlayTrigger></li>
                </ul>
            </div>
            <div style={{
                width: "93%",
                float: "left",
                padding: "5px 10px"
            }}>
                <div style={{ width: "70%", margin: "0 auto" }}>
                    <label className="input-label">
                        User Name
                        <input type="text" name="userName" defaultValue={userData.user_name} onChange={(value) => setUserInfo(value, 'user_name')} />
                    </label>
                    <label className="input-label">
                        User Mars Ad Name
                        <input type="text" name="userMarsAdName" defaultValue={userData.user_mars_ad_name} onChange={(value) => setUserInfo(value, 'user_mars_ad_name')} />
                    </label>
                    <label className="input-label">
                        User Email
                        <input type="text" name="userEmail" defaultValue={userData.user_email} onChange={(value) => setUserInfo(value, 'user_email')} />
                    </label>
                    <label className="input-label">
                        User Password
                        <input type="text" name="userPassword" defaultValue={userData.user_password} onChange={(value) => setUserInfo(value, 'user_password')} />
                    </label>
                    <div>
                        <table style={{ width: "100%" }}>
                            <tr>
                                <td style={personaTable}> User Persona</td>
                                <td style={personaTable}>Segment</td>
                            </tr>
                            {
                                selectedPersona.map((p, index) => {
                                    return (<tr>
                                        <td style={personaTable}>
                                            <select style={{ width: "90%", height: "30px" }} onChange={value => selectPersona(value, p)} value={p.key}>
                                                <option value="" disabled selected hidden>  Select Persona</option>
                                                {
                                                    persona.map((y) => {
                                                        return (<option key={y} value={y}>{y}</option>)
                                                    })
                                                }
                                            </select>
                                        </td>
                                        <td style={segmentDropDown}>
                                            <MultiSelect
                                                options={segments}
                                                value={p.value}
                                                onChange={(name) => selectedSegment(name, p)}
                                                labelledBy="Select"
                                            />
                                        </td>
                                        <td onClick={(value) => deleteSelectedPersona(value, index)} style={deletePersona}>X </td>
                                    </tr>)
                                })
                            }
                        
                        </table>
                        <button onClick={(value) => addPersona(value)}>+ Add Persona</button>
                    </div>


                    <div style={{ textAlign: "center" }}>
                        <button onClick={(value) => saveUser(value)}>Save</button>
                        <button>Cancel</button>
                    </div>
                </div>

            </div>

        </div>
        // <div className="popup-box" style={{ zIndex: "100" }}>
        //     <div className="box">
        //         <div style={{ padding: "15px", backgroundColor: "8080cf" }}>
        //             <span className="close-icon" onClick={props.handleClose}>x</span>
        //             <h3 style={{ margin: "0" }}>User Persona</h3>
        //         </div>
        //         <div style={{ marginTop: "1%" }}>
        //             <input type="radio" id="Lead" defaultChecked name="persona" value="View Persona" onChange={(value) => setPersonaView(value, 'View Personal')} />
        //             <label>View Persona</label>
        //             <input type="radio" id="css" name="persona" value="Create Persona" onChange={(value) => setPersonaView(value, 'Create Persona')} />
        //             <label>Create Persona</label>
        //             <input type="radio" id="css" name="persona" value="Delete Persona" onChange={(value) => setPersonaView(value, 'Delete Persona')} />
        //             <label>Delete Or Update Persona</label>
        //         </div>
        //         <div>

        //         </div>
        //         <div className="modal-box">
        //             {
        //                 showCreatePersona === true &&
        //                 <div>
        //                 <label className="input-label">
        //                     User Name
        //                     <input type="text" name="userName" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Mars Ad Name
        //                     <input type="text" name="marsAdName" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Email
        //                     <input type="text" name="userEmail" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Password
        //                     <input type="text" name="userPassword" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Persona
        //                     <MultiSelect
        //                         options={persona}
        //                         value={selectedPersonaName}
        //                         onChange={(name) => selectedPersona(name)}
        //                         labelledBy="Select"
        //                     />
        //                 </label>
        //                 <label className="input-label">
        //                     Segment
        //                     <MultiSelect
        //                         options={segments}
        //                         value={selectedSegmentName}
        //                         onChange={(name) => selectedSegment(name)}
        //                         labelledBy="Select"
        //                     />
        //                 </label>
        //                 <div style={{ textAlign: "center" }}>
        //                     <button>Save</button>
        //                     <button>Cancel</button>
        //                 </div>
        //             </div>

        //             }
        //             {
        //                 showViewPersona === true &&
        //                 <div>
        //                     View Persona
        //                 <label className="input-label">
        //                     User Name
        //                     <input type="text" name="userName" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Mars Ad Name
        //                     <input type="text" name="marsAdName" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Email
        //                     <input type="text" name="userEmail" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Password
        //                     <input type="text" name="userPassword" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Persona
        //                     <MultiSelect
        //                         options={persona}
        //                         value={selectedPersonaName}
        //                         onChange={(name) => selectedPersona(name)}
        //                         labelledBy="Select"
        //                     />
        //                 </label>
        //                 <label className="input-label">
        //                     Segment
        //                     <MultiSelect
        //                         options={segments}
        //                         value={selectedSegmentName}
        //                         onChange={(name) => selectedSegment(name)}
        //                         labelledBy="Select"
        //                     />
        //                 </label>
        //                 <div style={{ textAlign: "center" }}>
        //                     <button>Save</button>
        //                     <button>Cancel</button>
        //                 </div>
        //             </div>

        //             }
        //             {
        //                 showDeletePersona === true &&
        //                 <div>
        //                     Delete Persona
        //                 <label className="input-label">
        //                     User Name
        //                     <input type="text" name="userName" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Mars Ad Name
        //                     <input type="text" name="marsAdName" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Email
        //                     <input type="text" name="userEmail" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Password
        //                     <input type="text" name="userPassword" />
        //                 </label>
        //                 <label className="input-label">
        //                     User Persona
        //                     <MultiSelect
        //                         options={persona}
        //                         value={selectedPersonaName}
        //                         onChange={(name) => selectedPersona(name)}
        //                         labelledBy="Select"
        //                     />
        //                 </label>
        //                 <label className="input-label">
        //                     Segment
        //                     <MultiSelect
        //                         options={segments}
        //                         value={selectedSegmentName}
        //                         onChange={(name) => selectedSegment(name)}
        //                         labelledBy="Select"
        //                     />
        //                 </label>
        //                 <div style={{ textAlign: "center" }}>
        //                     <button>Save</button>
        //                     <button>Cancel</button>
        //                 </div>
        //             </div>

        //             }

        //         </div>
        //     </div>
        // </div>
    );
};

export default CreateUser;