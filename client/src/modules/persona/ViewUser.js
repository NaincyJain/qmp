import React, { useState, useEffect } from "react";
import yearlyTarget from '../../images/metric_list_yearly_target.png';
import metrixInput from '../../images/metrix_insight_input.png';
import metrixView from '../../images/metrix_insight_view.png';
import reportingLayout from '../../images/reporting_layouts.png';
import reportingTimelines from '../../images/reporting_timelines.png';
import rrMatrix from '../../images/rr_matrix.png';
import home from '../../home.png';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import HomeService from "../../services/HomeService";
import deleteUserData from "../../services/HomeService";
import {
    BrowserRouter,
    useRouteMatch,
    Switch,
    Route,
    Link,
    useHistory
} from "react-router-dom";
import EditUserPopup from "./EditUserPopup";

function ViewUser(props) {
    const history = useHistory();
    const [allUser, setAllUser] = useState([]);
    const [loading, setLoading] = useState(false);
    const homeService = new HomeService;
    useEffect(() => {
        // Update the document title using the browser API
        console.log("Use Effect called")
        if (allUser.length === 0) {
            setLoading(true);
            homeService.getAllUser().then(item => {
                setLoading(false);
                var data = JSON.parse(item.data);
                console.log(data);
                setAllUser(data);
            })
        }
    });
    const optionImage = {
        width: "100%",
        // border: "1px solid",
        // padding: "10px",
        // borderRadius: "8px",
        cursor: "pointer",
        margin: "10px 0"
    }
    const personaTable = {
        border: "none",
        padding: "0",
        fontSize: "14px",
        width: "50%"
    }


    const deleteUserRow = async (user) => {
        let deleteBody = {
            "u_id": user.User_id
        }
        console.log("deleteUser", user, deleteBody);
        // window.location.reload(true);
        try {
            setLoading(true);
            let delete_response = await homeService.deleteUserData(deleteBody)
            let item = await homeService.getAllUser()
            setLoading(false);
            let datas = JSON.parse(item.data)
            console.log(datas);
            setAllUser(datas);
        }
        catch (error) {
            console.log(error, "Error while deleting")
        }
    }

    const updateUser = async () => {
        console.log("updating user")
        setLoading(true);
        let item = await homeService.getAllUser()
        setLoading(false);
        let datas = JSON.parse(item.data)
        console.log(datas);
        setAllUser(datas);
    }


    return (
        <div>
            {loading && <div className="loader-box">
                <div id="loader"></div>
            </div>}

            <div style={{
                width: "3%",
                float: "left",
                padding: "5px 10px",
                height: "85vh",
                borderRight: "1px solid #e2dede"
            }}>
                <ul style={{ listStyle: "none", paddingInlineStart: "10px" }}>
                    <li onClick={() => history.push('/mainMenu')}>
                        <OverlayTrigger
                            key="mainMenu"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-metricList" className="my-tooltip">
                                    Home
                                </Tooltip>
                            }
                        >
                            <img src={home} style={optionImage} />
                        </OverlayTrigger>

                    </li>
                    <li onClick={() => history.push('/dashboard/metricList')}>
                        <OverlayTrigger
                            key="metricList"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-metricList" className="my-tooltip">
                                    Corporate Metrics & Annual Targets
                                </Tooltip>
                            }
                        >
                            <img src={yearlyTarget} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/rr_matrix')}>
                        <OverlayTrigger
                            key="rrMatrix"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-rrMatrix" className="my-tooltip">
                                    R&R Matrix
                                </Tooltip>
                            }
                        >
                            <img src={rrMatrix} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/metricInput')}>
                        <OverlayTrigger
                            key="metricInput"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-metricInput" className="my-tooltip">
                                    Metrics + Insight Input
                                </Tooltip>
                            }
                        >
                            <img src={metrixInput} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/metricView')}>
                        <OverlayTrigger
                            key="metricView"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-metricView" className="my-tooltip">
                                    Metrics + Insights View
                                </Tooltip>
                            }
                        >
                            <img src={metrixView} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/reportingTimeline')}>
                        <OverlayTrigger
                            key="reportingTimeline"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-reportingTimeline" className="my-tooltip">
                                    Reporting Timelines
                                </Tooltip>
                            }
                        >
                            <img src={reportingTimelines} style={optionImage} />
                        </OverlayTrigger></li>
                    <li onClick={() => history.push('/dashboard/reportingLayout')}>
                        <OverlayTrigger
                            key="reportingLayout"
                            placement="right"
                            overlay={
                                <Tooltip id="tooltip-reportingLayout" className="my-tooltip">
                                    Reporting Layout
                                </Tooltip>
                            }
                        >
                            <img src={reportingLayout} style={optionImage} />
                        </OverlayTrigger></li>
                </ul>
            </div>
            <div style={{
                width: "93%",
                float: "left",
                padding: "5px 10px"
            }}>
                <div style={{ width: "70%", margin: "0 auto", paddingBottom: '30px' }}>
                    <table>
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>User Email</th>
                                <th>User Mars Ad Name</th>
                                <th>User Persona Name</th>
                                <th>Segment Name</th>
                                <th>Delete User</th>
                                <th>Edit User</th>
                            </tr>

                        </thead>
                        <tbody>
                            {
                                allUser.map(user => {
                                    return (<tr>
                                        <td>{user.User_name}</td>
                                        <td>{user.User_email}</td>
                                        <td>{user.User_mars_ad_name}</td>
                                        <td>{user.User_persona_name}</td>
                                        <td>{user.Segment_name}</td>
                                        <td><button onClick={() => deleteUserRow(user)} style={{ cursor: 'pointer', whiteSpace: 'nowrap' }}>Delete User</button></td>
                                        <td>
                                            <EditUserPopup
                                                userName={user.User_name}
                                                userEmailId={user.User_email}
                                                userId={user.User_id}
                                                updateUserList={updateUser}
                                            />
                                        </td>
                                    </tr>)
                                })
                            }
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    );
};

export default ViewUser;