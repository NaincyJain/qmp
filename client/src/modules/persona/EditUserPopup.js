import React, { useState, useEffect } from "react";
import '../Popup.css';
import Creatable from 'react-select/creatable'
import MultiSelect from "react-multi-select-component";
import { Button, Dialog } from '@material-ui/core';
import HomeService from "../../services/HomeService";


const EditUserPopup = props => {
    const [userName, setUserName] = useState(props.userName);
    const [userEmailId, setUserEmailId] = useState(props.userEmailId);
    const [userId, setUserId] = useState(props.userId);
    const [open, setOpen] = useState(false);
    const [loading, setLoading] = useState(false);

    const [allUser, setAllUser] = useState([]);
    const homeService = new HomeService;

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    const editUserRow = async (user) => {
        setOpen(false);
        let editBody = {
            "u_id": userId,
            "u_mail": userEmailId,
            "u_name": userName
        }
        console.log("editUser", user, editBody);
        setLoading(true);
        // window.location.reload(true);
        try {
            let edit_response = await homeService.editUserData(editBody)
            setLoading(false);
        }
        catch (error) {
            console.log(error, "Error while deleting")
        }
        props.updateUserList()
        // let item = await homeService.getAllUser()
        // let datas = JSON.parse(item.data)
        // console.log(datas);
        // setAllUser(datas);
    }

    const handleUsernameChange = (e) => {
        console.log("event", e)
        setUserName(e?.target.value)
    }
    const handleUseremailChange = (e) => {
        console.log("event", e)
        setUserEmailId(e?.target.value)
    }

    // useEffect(() => {
    //     // Update the document title using the browser API
    //     console.log("Use Effect called")
    //     if (allUser.length === 0) {
    //         homeService.getAllUser().then(item => {
    //             var data = JSON.parse(item.data);
    //             console.log(data);
    //             setAllUser(data);
    //         })
    //     }
    // });


    return (
        <div>
            {loading && <div className="loader-box">
                <div id="loader"></div>
            </div>}
            <div>
                <button onClick={handleClickOpen} style={{ cursor: 'pointer', whiteSpace: 'nowrap' }}>Edit User</button>
            </div>
            <Dialog
                open={open}
                onClose={handleClose}
            >
                <div className="popup-box">
                    <div className="box">
                        <div style={{ padding: "15px", backgroundColor: "8080cf" }}>
                            <span className="close-icon" onClick={handleClose}>x</span>
                            <h3 style={{ margin: "0" }}>Edit User</h3>
                        </div>
                        <div>

                        </div>
                        <div className="modal-box">
                            <label className="input-label">User Name
                                <input type="text" value={userName} onChange={handleUsernameChange} />
                            </label>
                            <label className="input-label">User Email
                                <input type="text" value={userEmailId} onChange={handleUseremailChange} />
                            </label>
                            <div style={{ textAlign: "center" }}>
                                <button style={{ cursor: 'pointer' }} onClick={editUserRow}>Save</button>
                                <button style={{ cursor: 'pointer' }} onClick={handleClose}>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Dialog>
        </div>
    );
};

export default EditUserPopup;