import React, { useState } from "react";
import './Popup.css';
import HomeService from "../services/HomeService";
const EditTarget = props => {
  const [target, setTarget] = useState(props.target);
  const [loading, setloading] = useState(false);
  console.log(target);
  let body = JSON.parse(JSON.stringify(target));
  const [segment, setSegment] = useState([]);
  const [user, setUser] = useState({});
  const homeService = new HomeService;



  body.Segment.forEach(element => {
    for (var key in element) {
      for (var k in element[key]) {
        if (k == 'CYT') {
          var obj = {};
          obj.segmentName = key;
          obj.value = element[key][k];
          obj.comments = "";
          obj.allowEdit = element[key]['allowEdit'];
          segment.push(obj);
        }
      }
    }
  });
  console.log(segment);
  const saveEditedSegment = () => {
    //let body = {};
    setloading(!loading);
    segment.forEach(item => {
      var key = item.segmentName + " CYT"
      var commentKey = item.segmentName + " Comments"
      body[key] = item.value
      body[commentKey] = item.comments
    })

    body.Segment.forEach(element => {
      for (var key in element) {
        for (var k in element[key]) {
          if (k != 'CYT' && k != 'Comments' && k != 'allowEdit') {
            body[key + " " + k] = element[key][k]
          }
        }
      }
    })
    delete body["Segment"]
    delete body["targets"]
    delete body["targetTemp"]
    delete body["allowEdit"]
    delete body["currentYearSelected"]
    homeService.editTarget(body).then(item => {
      body["allowEdit"] = target.allowEdit;
      body["currentYearSelected"] = target.currentYearSelected;
      setloading(!loading);
      props.sendDataToParent(body);
    })
  }

  const handleChange = (item, e, type) => {
    // item.value = e.target.value;
    // console.log(item);
    console.log(e);
    if (type == 'Comments') {
      segment.forEach(i => {
        if (i.segmentName == item.segmentName) {
          item.comments = e
          console.log(item);
        }
      })
      setSegment(segment);
    } else {
      console.log("Else block");
      segment.forEach(item => {
        if (type == item.segmentName) {
          item.value = e
          console.log(item);
        }
      })
      setSegment(segment);
    }

  }
  return (
    <div>
      { loading && <div className="loader-box">
                <div id="loader"></div>
      </div>}
      <div className="modal-box">
        <table style={{ margin: "1% auto" }}>
          <thead>
            <tr>
              <th style={{ position: "inherit" }}>Segment</th>
              <th style={{ position: "inherit" }}>Target</th>
              <th style={{ position: "inherit" }}>Comments</th>
            </tr>
          </thead>
          <tbody>
            {
              segment.map(item => {
                if (item.allowEdit == true) {
                  return (<tr>
                    <td> {item.segmentName}</td>
                    <td> <input type="text" defaultValue={item.value} onChange={e => handleChange(item, e.target.value, item.segmentName)} /> </td>
                    <td> <textarea type="text" defaultValue={item.comments} onChange={e => handleChange(item, e.target.value, 'Comments')} /> </td>
                  </tr>
                  )
                } else {
                  return (<tr>
                    <td> {item.segmentName}</td>
                    <td> <input type="text" disabled value={item.value} /> </td>
                    <td> <textarea type="text" disabled value={item.comments} /> </td>
                  </tr>
                  )
                }

              })
            }


          </tbody>

        </table>

        <div style={{ textAlign: "center" }}>
          <button onClick={saveEditedSegment}>Save</button>
          <button onClick={props.setModalIsOpenToFalse}>Cancel</button>
        </div>

      </div> 
    </div>



  );
};

export default EditTarget;