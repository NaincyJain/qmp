import { makeStyles } from '@material-ui/core';
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/styles';
import { Dialog, Snackbar } from '@material-ui/core'


const useStyles = makeStyles(() => ({
  header: {
    backgroundColor: "#00d7b9",
    color: "#0000a0",
    height: '300px',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'column',
    gap: '30px',
    fontFamily: 'MarsCentra-Bold'
  },
  linkStyle: {
    color: "#0000a0",
    fontSize: '18px',
    fontFamily: 'MarsCentra-Bold'

  },
}))

const LightTooltip = withStyles(() => ({
  tooltip: {
    backgroundColor: '#484848',
    color: 'white',
    // boxShadow: theme.shadows[1],
    fontSize: 12,
  },
}))(Tooltip);

function ErrorPage() {

//   const { favState, favDispatch, disableFavButton, setDisableFavButton } = useContext(GlobalContext);


  const classes = useStyles();

  const [ErrorDialog, setErrorDialog] = useState(true)
  function handleClose() {
    setErrorDialog(false);
  }

//   useEffect(() => {
//     favDispatch({ type: 'WELCOMEBANNER', payload: false });
//   }, [])

  return (
    <div>
      <Dialog fullScreen open={true} onClose={handleClose} >
        <div className={classes.header}>
          <span style={{fontSize: '40px'}}>404 Page Not Found!</span>
          <span style={{fontSize: '20px'}}>Looks like you have entered an incorrect URL</span>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center', paddingTop: '55px' }}>
          <LightTooltip title="Main Menu" >
            <Link to="/mainMenu" className={classes.linkStyle}>
              Click here to go to Main Menu
            </Link>
          </LightTooltip>
        </div>
      </Dialog>
    </div>
  )
}
export default ErrorPage;