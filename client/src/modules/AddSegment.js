import React , {useState} from "react";
import './Popup.css';
const AddSegment = props => {
  const [newSegment, setNewSegment] = useState({segment:'',comment:'', 'product/service': props.metricType});
  const changeInput = (value,opt) => {
    if(opt == 'Segment'){
      newSegment.segment = value.target.value
    }else{
      newSegment.comment = value.target.value
    }
  }
  const setProductService = (value,option) =>{
    console.log(option);
    newSegment["product/service"] = option;

  }
  const saveSegment = (value) =>{
    console.log(newSegment);
    props.saveNewSegment(newSegment);
  }
  return (
    <div className="popup-box">
      <div className="box">
          <div style={{padding: "15px", backgroundColor : "#8080cf"}}>
             <span className="close-icon" onClick={props.handleClose}>x</span>
             <h3 style={{margin: "0"}}>Add New Segment</h3>
          </div>
          <div className="modal-box">
          <label className="input-label">
          New Segment
          <input type="text" name="segmentName" onChange={(value)=> changeInput(value,'Segment')}/>
            </label>
            <label className="input-label">
            Comments
            <textarea style={{display: "block",width: "100%"}} type="text" name="comment" onChange={(value)=> changeInput(value,'Comment')}/>
            </label>
            <label className="input-label">
              Type
              <div style={{marginTop: "1%"}}>
              <input type="radio" id="Lead" name="Product_Service" checked={newSegment['product/service'] === "Product"} value="Product" onChange={(value)=> setProductService(value,'Product')} />
              <label>Product</label>
              <input type="radio" id="css" name="Product_Service" checked={newSegment['product/service'] === "Service"} value="Service" onChange={(value)=> setProductService(value,'Service')} />
              <label>Service</label>
              </div>
              
            </label>
            <div style={{textAlign: "center"}}>
            <button onClick={(value)=> saveSegment(value)}>Save</button>
            <button onClick={props.handleClose}>Cancel</button>
            </div>
            
          </div>
      </div>
    </div>
  );
};
 
export default AddSegment;