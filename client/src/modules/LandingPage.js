import yearlyTarget from '../images/metric_list_yearly_target.png';
import metrixInput from '../images/metrix_insight_input.png';
import metrixView from '../images/metrix_insight_view.png';
import reportingLayout from '../images/reporting_layouts.png';
import reportingTimelines from '../images/reporting_timelines.png';
import rrMatrix from '../images/rr_matrix.png';
import { useHistory } from 'react-router-dom';
import DateRangeOutlinedIcon from '@mui/icons-material/DateRangeOutlined';
import DescriptionOutlinedIcon from '@mui/icons-material/DescriptionOutlined';
import ListAltOutlinedIcon from '@mui/icons-material/ListAltOutlined';
import AssignmentOutlinedIcon from '@mui/icons-material/AssignmentOutlined';
import PostAddOutlinedIcon from '@mui/icons-material/PostAddOutlined';
import GridOnOutlinedIcon from '@mui/icons-material/GridOnOutlined';
import NoteAddOutlinedIcon from '@mui/icons-material/NoteAddOutlined';
function LandingPage() {

  const history = useHistory();
  const optionHeading = {
    margin: "0",
    fontWeight: "600",
    fontSize: "16px"
  }
  const optionImage = {
    // width: "10%",
    border: "2px solid",
    padding: "10px",
    borderRadius: "8px",
    cursor: "pointer",
    fontSize: '48',
    color: 'black',
    //margin: "10px 0"
  }
  return (

    <div>
      <div className="grid-container">
        <div className="grid-item" onClick={() => history.push('/dashboard/metricList')}>
          {/* <img src={yearlyTarget} style={optionImage} ></img> */}
          <AssignmentOutlinedIcon style={optionImage} />
          <h6 style={optionHeading}>Corporate Metrics & Annual Targets</h6>
        </div>
        <div className="grid-item" onClick={() => history.push('/dashboard/rr_matrix')}>
          {/* <img src={rrMatrix} style={optionImage} ></img> */}
          <GridOnOutlinedIcon style={optionImage} />
          <h6 style={optionHeading}>Corporate Metrics Roles & Responsibility Matrix</h6>
        </div>
        <div className="grid-item" onClick={() => history.push('/dashboard/metricInput')}>
          {/* <img src={metrixInput} style={optionImage}></img> */}
          <PostAddOutlinedIcon style={optionImage} />
          <h6 style={optionHeading}>Corporate Metrics Data & Insights Input</h6>
        </div>
        <div className="grid-item" onClick={() => history.push('/dashboard/metricView')}>
          {/* <img src={metrixView} style={optionImage} ></img> */}
          <ListAltOutlinedIcon style={optionImage} />
          <h6 style={optionHeading}>Corporate Metrics Data & Insight Output</h6>
        </div>
        <div className="grid-item" onClick={() => history.push('/dashboard/reportingTimeline')}>
          {/* <img src={reportingTimelines} style={optionImage}></img> */}
          <DateRangeOutlinedIcon style={optionImage} />
          <h6 style={optionHeading}>Corporate Reporting Timelines</h6>
        </div>
        <div className="grid-item" onClick={() => history.push('/dashboard/reportingLayout')}>
          {/* <img src={reportingLayout} style={optionImage}></img> */}
          <DescriptionOutlinedIcon style={optionImage} />
          <h6 style={optionHeading}>Reports</h6>
        </div>
        {/* <div className="grid-item" onClick={() => history.push('/dashboard/cdc')}>
          <img src={reportingLayout} style={optionImage}></img>
          <NoteAddOutlinedIcon style={optionImage} />
          <h6 style={optionHeading}>CDC Page</h6>
        </div> */}

      </div>
    </div>

  );



}

export default LandingPage;