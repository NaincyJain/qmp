import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Common from './Common';
 
// handle the public routes
function PublicRoute({ component: Component, ...rest }) {
    const commonService = new Common;
    // const token = commonService.getToken();
    let token = "";
    commonService.getToken().then(item =>{
        console.log("======",item !== null);
        token = item;
    })
    
    
  return (
    <Route
      {...rest}
      render={(props) => token !== null ? <Component {...props} /> : <Redirect to={{ pathname: '/mainMenu' }} />}
    />
  )
}
 
export default PublicRoute;