export const constants = {
    access_token: 'mars.access_token',
    refresh_token: 'mars.refresh_token',
    username: 'mars.username'
}

export default {
    welcomeBanner:true,
};

const favs = (state, action) => {
    // debugger;
    switch (action.type) {
      case "WELCOMEBANNER":
        return { ...state, welcomeBanner: action.payload }; 
        
      default:
        return state;
    }
  };
  
  export default favs;