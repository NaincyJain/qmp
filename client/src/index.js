import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import * as serviceWorker from './serviceWorker';
import {
  BrowserRouter,
  Route,
  Switch
} from "react-router-dom";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import LandingPage from './modules/LandingPage';
import Dashboard from './modules/Dashboard';
import Footer from './modules/Footer';
import Login from './modules/Login';
import Header from './modules/Header';
import PrivateRoute from './utils/PrivateRoute';
import PublicRoute from './utils/PublicRoute';
import CreateUser from './modules/persona/CreateUser';
import ViewUser from './modules/persona/ViewUser';
// import store from './redux/store';
// const Header = () =>{
//   const location = useLocation();
//   console.log(location);
//   return( <div> Header </div>)
// }


ReactDOM.render(<App />, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
// serviceWorker.register();

