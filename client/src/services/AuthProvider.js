
import React, { Component } from 'react';
import { msalConfig, msalApp, requiresInteraction, isIE } from './msalConfig';

// If you support IE, our recommendation is that you sign-in using Redirect APIs
const useRedirectFlow = isIE();

export default C =>
  class AuthProvider extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loading: true,
        account: null,
        admin: false,
        userId: null,
        error: null,
      };
    }

    async acquireToken(request, redirect) {
      return msalApp.acquireTokenSilent(request).catch(error => {
        // Call acquireTokenPopup (popup window) in case of acquireTokenSilent failure
        // due to consent or interaction required ONLY
        if (requiresInteraction(error.errorCode)) {
          return redirect
            ? msalApp.acquireTokenRedirect(request)
            : msalApp.acquireTokenPopup(request);
        } else {
          console.error('Non-interactive error:', error);
        }
      });
    }

    async onSignIn(redirect) {
      const account = msalApp.getAllAccounts();
      if (account.length === 1) {
        this.setState({
          account: account[0],
          error: null,
        });
      } else {
        if (redirect) {
          return msalApp.loginRedirect({
            scopes: msalConfig.scopes,
          });
        }
        const loginResponse = await msalApp
          .loginPopup({
            scopes: msalConfig.scopes,
            prompt: 'select_account',
          })
          .catch(error => {
            console.log('Sign In error: ', error);
            this.setState({
              error: error.message,
            });
          });

        if (loginResponse) {
          this.setState({
            account: loginResponse.account,
            error: null,
          });

        }
      }
    }

    onSignOut() {
      msalApp.logout();
    }

    render() {
      return (
        <C
          {...this.props}
          loading={this.state.loading}
          accessToken={this.state.accessToken}
          admin={this.state.admin}
          userId={this.state.userId}
          graphProfile={this.state.graphProfile}
          account={this.state.account}
          error={this.state.error}
          onSignIn={() => this.onSignIn(useRedirectFlow)}
          onSignOut={() => this.onSignOut()}
        />
      );
    }
  };