
class HomeService {

    async loginUser(data) {
        console.log(process.env.REACT_APP_BACKEND_API)
        const url = process.env.REACT_APP_BACKEND_API+"/loginuser";

        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json" },
            body: JSON.stringify(data)
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async getUserRoles(token) {
        const url = process.env.REACT_APP_BACKEND_API+"/userrole";
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async getMetrixData() {
        const url = process.env.REACT_APP_BACKEND_API+"/displaydata";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            console.log(response);
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async getCategoryList() {
        const url = process.env.REACT_APP_BACKEND_API+"/categoryfilter";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            console.log(response);
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async deleteMetrix(matrixId) {
        const url = process.env.REACT_APP_BACKEND_API+"/deletemetric";
        const token = JSON.parse(sessionStorage.getItem('token'));
        var obj = {
            metric_id: matrixId
        }
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(obj)
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async deleteSegment(segment) {
        const url = process.env.REACT_APP_BACKEND_API+"/deletesegment";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(segment)
        }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async saveSegment(segment) {
        const url = process.env.REACT_APP_BACKEND_API+"/addsegment";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(segment)
        }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async saveMetrix(mertic) {
        const url = process.env.REACT_APP_BACKEND_API+"/addmetric";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(mertic)
        }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async copyMetrix() {
        const url = process.env.REACT_APP_BACKEND_API+"/copymetrics";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async editTarget(target) {
        const url = process.env.REACT_APP_BACKEND_API+"/edittarget";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(target)
        }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async getYearFilter() {
        const url = process.env.REACT_APP_BACKEND_API+"/yearfilter";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async filterDataByYear(year,metricType) {
        const url = process.env.REACT_APP_BACKEND_API+"/yearselection?year=" + year+"&category="+metricType;
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async getRrmatrixDisplayData(data) {
        const url = process.env.REACT_APP_BACKEND_API+"/rrmatrix_display_data?year=" + data.year+"&category="+data.category;
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async editRrmatrixRoles(roles) {
        const url = process.env.REACT_APP_BACKEND_API+"/rrmatrix_editroles";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(roles)
        }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async displayMetricsinputscreen(user) {
        const url = process.env.REACT_APP_BACKEND_API+"/metricsinputscreen";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(user)
        }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async editPerformance(data) {
        const url = process.env.REACT_APP_BACKEND_API+"/editperformance";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(data)
        }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async displayMetricsViewScreen() {
        const url = process.env.REACT_APP_BACKEND_API+"/displaydatascreen4";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            console.log(response);
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async displayTimelineData() {
        const url = process.env.REACT_APP_BACKEND_API+"/displaytimeline";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async editTimeline(data) {
        const url = process.env.REACT_APP_BACKEND_API+"/edittimeline";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(data)
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }

    async displayDetailedReport(data) {
        const url = process.env.REACT_APP_BACKEND_API+"/displaydata_screen6?year="+data.Year+"&quarter="+data.Quarter+"&category="+data.type;
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token }
        }).then(response => {
            // if (response.status === 400) {
            //     this.goToLoginPage();
            // }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async displayOverviewReport(data) {
        const url = process.env.REACT_APP_BACKEND_API+"/displaydata_overviewreport?year="+data.Year+"&quarter="+data.Quarter;
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token }
        }).then(response => {
            // if (response.status === 400) {
            //     this.goToLoginPage();
            // }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async filterDataByYearInsightView(year,type) {
        const url = process.env.REACT_APP_BACKEND_API+"/yearfilterscreen4?year=" + year+"&category="+type;
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, { method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token } }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async editMetricInsightPerformance(data) {
        const url = process.env.REACT_APP_BACKEND_API+"/editscreen4";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(data)
        }).then(response => {
            if(response.status === 400){
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async getSegmentList() {
        const url = process.env.REACT_APP_BACKEND_API+"/segment_list";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token }
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async addNewUser(data) {
        const url = process.env.REACT_APP_BACKEND_API+"/add_new_user";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token }, 
            body: JSON.stringify(data)
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async getAllUser() {
        const url = process.env.REACT_APP_BACKEND_API+"/display_system_users";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token }
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    async getUserList() {
        const url = process.env.REACT_APP_BACKEND_API+"/users_list";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "GET", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token }
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
    goToLoginPage() {
        //alert("Hello");
        window.location.href = "/login"
    }

    handleResponseError(response) {
        throw new Error("HTTP error, status = " + response.status);
    }

    handleError(error) {
        console.log(error.message);
    }
    
    async deleteUserData(userId) {
        const url = process.env.REACT_APP_BACKEND_API+"/deleteuser";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(userId)
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }
        
    async editUserData(userId) {
        const url = process.env.REACT_APP_BACKEND_API+"/edituser";
        const token = JSON.parse(sessionStorage.getItem('token'));
        return fetch(url, {
            method: "POST", mode: "cors", headers: { "Content-Type": "application/json", "x-access-tokens": token },
            body: JSON.stringify(userId)
        }).then(response => {
            if (response.status === 400) {
                this.goToLoginPage();
            }
            if (!response.ok) {
                this.handleResponseError(response);
            }
            return response.json();
        }).catch(error => {
            this.handleError(error);
        })
    }

}
export default HomeService;