import pandas as pd
from pandas.io import json
import pyodbc
import datetime 
import json as js1
from flask import jsonify
from flask import request
from collections import defaultdict
from collections import OrderedDict
from functools import wraps
import jwt
from datetime import date
from app import *

#When in doubt, print it out.

# server = 'mars-horizon.database.windows.net'
# database = 'marsanalyticsdevsqlsrv'
# username = 'mthco'
# password = 'Mathco_123'
server = 'marsanalyticsdevsqlsrv.database.windows.net'
database = 'qmpdevsqldb'
username = 'BI_User'
password = 'B}Gvd,9@pHW%5(b6'


def login_function(uname,pswd):
    #Check if username and password are present in the system
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute('''SELECT [User_id] FROM dbo.tbl_user_personas_V2 WHERE 
                    (User_password = ? and User_name = ?) or 
                    (User_password = ? and User_mars_ad_name = ?)''', (pswd, uname, pswd, uname))
    result = cursor.fetchall()
    #result = list(result[0])
    print(result)
    if len(result) == 0:
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
        cursor = cnxn.cursor()
        cursor.execute('''SELECT [User_id] FROM dbo.tbl_user_personas_V2 WHERE User_name = ? or User_mars_ad_name = ?''', (uname,uname))
        user = cursor.fetchall()
        if len(user) == 0:
            #return "User not in system. Please check details and try again"
            return "Not in system"
        else:
            #return "Username or password incorrect"    
            return "Incorrect data"
    result = list(result[0])
    return repr(result[0])


def get_roles(userid):
    #conn through pyodbc execute through pandas
    year = date.today().year
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query(f"""SELECT * FROM dbo.tbl_user_personas_V2 where User_id = {userid} and Year = {year}""",cnxn)
    cnxn.close()
    #getting column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute("Select top 0 * from dbo.tbl_user_personas_V2")
    colnames = [desc[0] for desc in cursor.description]
    cnxn.close()    
    #generating dataframe
    df = pd.DataFrame(sql_query, columns= colnames)
    #print(df.to_string())
    #Get distinct personas the user is in
    personas = list(df['User_persona_name'].unique())
    print(personas)
    #get the segments the user is assigned to in each persona
    user_access = {}
    if len(personas) > 1:
        if 'View-Only' in personas:
            personas.remove('View-Only')
    for i in personas:
        user_access[i] = {}
        segment = df['Segment_name'][df['User_persona_name'] == i]
        for j in list(segment):
            if j == "All":
                user_access[i][j] = [0]
            else:
                user_access[i][j] = []
    if len(personas) == 1:
        if 'View-Only' in personas:
            user_json = js1.dumps(user_access)
            return user_json
    if 'Corporate Admin' in personas:
        user_json = js1.dumps(user_access)
        return user_json
    if 'Corporate System ADMIN' in personas:
        user_json = js1.dumps(user_access)
        return user_json
    if 'Target Definition' in personas:
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
        sql_query_2 = pd.read_sql_query(f"""Select * from dbo.tbl_targets_definition_role_V2 where User_id = {userid} and Year = {year}""", cnxn)
        cnxn.close()
        #getting column names
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
        cursor = cnxn.cursor()
        cursor.execute("Select top 0 * from dbo.tbl_targets_definition_role_V2")
        columns = [desc[0] for desc in cursor.description]
        cnxn.close() 
        target_df = pd.DataFrame(sql_query_2, columns = columns)
        for i in user_access['Target Definition'].keys():
            metrics = target_df['Metric_id'][target_df['Segment_name'] == i]
            for metric in metrics:
                user_access['Target Definition'][i].append(metric)
    if 'Segment Data' or 'Segment Insights' or 'Corporate Insights' or 'Corporate Data' in personas:
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
        sql_query_3 = pd.read_sql_query(f"""Select * from dbo.tbl_user_inputs_role_V2 where User_id = {userid} and Year = {year}""", cnxn)
        cnxn.close()
        #getting column names
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
        cursor = cnxn.cursor()
        cursor.execute("Select top 0 * from dbo.tbl_user_inputs_role_V2")
        user_columns = [desc[0] for desc in cursor.description]
        cnxn.close() 
        roles_df = pd.DataFrame(sql_query_3, columns = user_columns)
        if 'Segment Data' in personas:
            for i in user_access['Segment Data'].keys():
                metrics = roles_df['Metric_id'][(roles_df['User_persona_name'] == 'Segment Data') & (roles_df['Segment_name'] == i)]
                for metric in metrics:
                    user_access['Segment Data'][i].append(metric)
        if 'Segment Insights' in personas:
            for i in user_access['Segment Insights'].keys():
                metrics = roles_df['Metric_id'][(roles_df['User_persona_name'] == 'Segment Insights') & (roles_df['Segment_name'] == i)]
                for metric in metrics:
                    user_access['Segment Insights'][i].append(metric)
        if 'Corporate Insights' in personas:
            for i in user_access['Corporate Insights'].keys():
                metrics = roles_df['Metric_id'][(roles_df['User_persona_name'] == 'Corporate Insights') & (roles_df['Segment_name'] == i)]
                for metric in metrics:
                    user_access['Corporate Insights'][i].append(metric)
        if 'Corporate Data' in personas:
            for i in user_access['Corporate Data'].keys():
                metrics = roles_df['Metric_id'][(roles_df['User_persona_name'] == 'Corporate Data') & (roles_df['Segment_name'] == i)]
                for metric in metrics:
                    user_access['Corporate Data'][i].append(metric)
    user_json = js1.dumps(user_access)
    return user_json       
