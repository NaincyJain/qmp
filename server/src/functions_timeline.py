from collections import OrderedDict
import pandas as pd
import pyodbc
import datetime
import json as js1
from pandas.io import json
from datetime import date
from flask import jsonify

#When in doubt, print it out.

# server = 'mars-horizon.database.windows.net'
# database = 'marsanalyticsdevsqlsrv'
# username = 'mthco'
# password = 'Mathco_123'
server = 'marsanalyticsdevsqlsrv.database.windows.net'
database = 'qmpdevsqldb'
username = 'BI_User'
password = 'B}Gvd,9@pHW%5(b6'


def displaytimelinefunction(): 
    #conn through pyodbc execute through pandas
    #and Year = 2021
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query('''SELECT * from dbo.tbl_timelines_v2''',cnxn)
    cnxn.close()
    #getting column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='tbl_timelines_v2'")
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_df = [item for sublist in column_name for item in sublist]
    cnxn.close()
    #converting to df
    df = pd.DataFrame(sql_query, columns= columns_df)
    #convert to json
    js = df.to_json(orient = 'records')
    return js


def updated_row(json):
    query = "Select * from dbo.tbl_timelines_v2 where step_id = " + f"{json['step_id']}"
    print(query)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute(query)
    r = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()
    js = js1.dumps(r[0])
    return js 


def edit_timeline(json):
    quarter_end = ['13', '3', '6', '9', '13']
    due_date_formula = json["reporting_due_date"]
    day_of_week = due_date_formula[-1:]
    current_year = date.today().year
    previous_year = date.today().year - 1
    next_year = date.today().year + 1
    if 'X+' in due_date_formula:
        variables = []
        for i in range(len(quarter_end)):
            due = due_date_formula.replace("X", quarter_end[i]) #converting PX+1 W2 D7 to P1+1 W2 D7
            period_calculation = due.replace("P","") #Removing P from above. 1+1 W2 D7
            period_calculation = period_calculation.split(' ')[0] #taking the first part of the formula : 1+1
            a = int(period_calculation.split('+')[0]) #splitting 1+1 and converting to int
            b = int(period_calculation.split('+')[1])
            period = "P" + str((a+b)%13).zfill(2) #Adding it up and bringing it to period format :P2
            if i == len(quarter_end)-1:
                final_period_value = str(next_year) + period + due.split(' ')[1] #converting to '2020P2W2' format
            else:
                final_period_value = str(current_year) + period + due.split(' ')[1] 
            variables.append(final_period_value)
    else:
        variables = []
        for i in range(len(quarter_end)):
            due = due_date_formula.replace("PX", quarter_end[i])
            if i == 0:
                final_period_value = str(previous_year) + 'P' + str(due.split(' ')[0]).zfill(2) + due.split(' ')[1]
            else:
                final_period_value = str(current_year) + 'P' + str(due.split(' ')[0]).zfill(2) + due.split(' ')[1]
            variables.append(final_period_value)
    print(variables)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    sqlExecSP = "exec dbo.[EDIT_TIMELINES] @PYQ4 = ?, @CYQ1 = ?, @CYQ2 = ?, @CYQ3 = ?, @CYQ4 = ?, @DAY_OF_WEEK = ?, @STEP_ID = ?, @DUE_DATE_FORMULA = ?"
    params = (variables[0], variables[1], variables[2], variables[3], variables[4], day_of_week, json['step_id'], due_date_formula)
    cursor.execute(sqlExecSP, params)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection


def update_timelines(json, firstresult):
    #update history table
    #get old value of metric
    column_names = json.keys()
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    old_value_query = pd.read_sql_query(("SELECT * FROM dbo.tbl_timelines_v2 WHERE step_id = " + repr(json['step_id'])), cnxn)
    cnxn.commit()
    cnxn.close() # Close Connection
    #convert old value and new value to json and add to history table
    df = pd.DataFrame(old_value_query, columns= column_names) 
    js = df.to_json(orient = 'records') #convert to json
    new_js = js1.dumps(json)
    #upate history table
    query = 'Select distinct user_name from dbo.user_ref where [ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.edit_timeline_history(old_value, new_value, when_updated, updated_by) values (?, ?, ?, ?)", js, new_js, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    #update table
    query = 'Select reporting_due_date from dbo.tbl_timelines_v2 where step_id = ' + repr(json['step_id'])
    print(query)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute(query)
    x = cursor.fetchall()
    current_tbl_value = x[0][0]
    print(x[0][0])
    if json['reporting_due_date'] == current_tbl_value:
        #update only the dates
        query = 'Update dbo.tbl_timelines_v2 set [PYQ4 Data Due Date] = ' + repr(json['PYQ4 Data Due Date']) + ', [CYQ1 Data Due Date] = ' + repr(json['CYQ1 Data Due Date']) + ', [CYQ2 Data Due Date] = ' + repr(json['CYQ2 Data Due Date']) + ', [CYQ3 Data Due Date] = ' + repr(json['CYQ3 Data Due Date']) + ', [CYQ4 Data Due Date] = ' + repr(json['CYQ4 Data Due Date']) + ' where step_id = ' + repr(json['step_id'])
        print(query)
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
        cursor = cnxn.cursor()
        cursor.execute(query)
        cnxn.commit()
        cursor.close() # Close and delete cursor
        del cursor
        cnxn.close() 
    else:
        edit_timeline(json)