import pandas as pd
from pandas.io import json
import pyodbc
from collections import defaultdict
from collections import OrderedDict
from functools import wraps
from datetime import date

#When in doubt, print it out.

# server = 'mars-horizon.database.windows.net'
# database = 'marsanalyticsdevsqlsrv'
# username = 'mthco'
# password = 'Mathco_123'
server = 'marsanalyticsdevsqlsrv.database.windows.net'
database = 'qmpdevsqldb'
username = 'BI_User'
password = 'B}Gvd,9@pHW%5(b6'

def segment_list():
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    current_date = date.today()
    current_year = current_date.year
    query = "select Segment from dbo.segment_ref where [Is Disabled] = 0 and [Year] = " + repr(current_year)
    cursor.execute(query)
    rows = cursor.fetchall()
    segment = []
    for row in rows:
        row_to_list = [elem for elem in row]
        segment.append(row_to_list)
    segment_list = [item for sublist in segment for item in sublist]
    js = json.dumps(segment_list)
    cnxn.close()
    return(js)

def delete_user(u_id):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    query = "delete from dbo.tbl_user_personas_V2 where [User_id] = " + repr(u_id)
    cursor.execute(query)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close()
    #update the user_ref table
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    query = "delete from dbo.user_ref where [ID] = " + repr(u_id)
    cursor.execute(query)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close()
    return "The user has been removed"

def edit_user(u_mail, u_name, u_id):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    query = "update dbo.tbl_user_personas_V2 set [User_email] = " + repr(u_mail) + ", [User_name] = " + repr(u_name) + " where [User_id] = " + repr(u_id)
    cursor.execute(query)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close()
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    query = "update dbo.user_ref set [user_name] = " + repr(u_name) + " where [User_id] = " + repr(u_id)
    cursor.execute(query)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close()
    return "Updated Sucessfully!"

def addnewuser(variable):
    current_year = date.today().year
    #Generating user id for the new user
    user_query = 'Insert into dbo.user_ref values(' + repr(variable['user_name']) + ')'
    print(user_query)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(user_query)
    cnxn.commit()
    cursor.close()
    #Fetch user_id of newly created user
    id_query = 'Select ID from dbo.user_ref where user_name = ' + repr(variable['user_name'])
    print(id_query)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(id_query)
    result = cursor.fetchall()
    user_id = result[0][0]
    cursor.close()
    #Generating data to initiate bulk insert
    row_values = []
    for i in variable.keys():
        if type(variable[i]) == list:
            print(i, " : ", variable[i])
            for segment in variable[i]:
                values = []
                values.append(user_id)
                values.append(variable["user_name"])
                values.append(variable["user_mars_ad_name"])
                values.append(variable["user_email"])
                values.append(variable["user_password"])
                values.append(0)
                values.append(i)
                values.append(0)
                values.append(segment)
                values.append(current_year)
                row_values.append(tuple(values))
    print(row_values)    
    insert_query = '''Insert into dbo.tbl_user_personas_V2(User_id, User_name, User_mars_ad_name, User_email, 
                        User_password, User_persona_id, User_persona_name, Segment_id, Segment_name, Year) 
                        values(?,?,?,?,?,?,?,?,?,?)'''    
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.executemany(insert_query, row_values)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    #Trigger SP
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    sqlExecSP = "exec dbo.[ADD_NEW_USER] @USER_ID = ?"
    params = (user_id)
    cursor.execute(sqlExecSP, params)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    return "Completed Successfully"

def display_users():
    year = date.today().year
    query = '''
            SELECT User_id, User_name, User_mars_ad_name, User_email, 
                User_persona_name = 
                STUFF((SELECT DISTINCT ', ' + User_persona_name
                       FROM dbo.tbl_user_personas_V2 b 
                       WHERE b.User_id = a.User_id 
                       FOR XML PATH('')), 1, 2, ''),
                Segment_name = 
                STUFF((SELECT DISTINCT ', ' + Segment_name
	                   FROM dbo.tbl_user_personas_V2 c
		               WHERE c.User_id = a.User_id
		               FOR XML PATH('')), 1, 2, '')
           FROM dbo.tbl_user_personas_V2 a
           WHERE Year = {}
           GROUP BY User_id, User_name, User_mars_ad_name, User_email
           '''
    print(query.format(year))
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query(query.format(year),cnxn)
    cnxn.close()
    #getting column names
    colnames = ['User_id', 'User_name', 'User_mars_ad_name', 'User_email', 'User_persona_name', 'Segment_name']
    #Converting to df
    df = pd.DataFrame(sql_query, columns= colnames)
    js = df.to_json(orient = 'records')
    return js
    


def users_list():
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    # current_date = date.today()
    # current_year = current_date.year
    query = "select user_name from dbo.user_ref"
    cursor.execute(query)
    rows = cursor.fetchall()
    segment = []
    for row in rows:
        segment.append([x for x in row])
    js = json.dumps(list(segment))
    cnxn.close()
    return(js) 