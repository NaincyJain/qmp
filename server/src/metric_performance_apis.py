import pyodbc
import pandas as pd
import json
import datetime
from datetime import date

#When in doubt, print it out.

# server = 'mars-horizon.database.windows.net'
# database = 'marsanalyticsdevsqlsrv'
# username = 'mthco'
# password = 'Mathco_123'
server = 'marsanalyticsdevsqlsrv.database.windows.net'
database = 'qmpdevsqldb'
username = 'BI_User'
password = 'B}Gvd,9@pHW%5(b6'


#This function refers to the marscalendar table to fetch the current quarter 
#   based on current data
def getquarter():
    query = "Select mars_quarter from dbo.marscalendar where gregorian_Calendar_day = '" + f"{date.today()}'" 
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute(query)
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_df = [item for sublist in column_name for item in sublist]
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    return columns_df[0]

def displaydata_metricperformance(userid):
    #Generating the select query based on current quarter
    current_quarter = getquarter()
    if current_quarter == 'Q1':
        year = date.today().year - 1
        query = '''Select Segment, Category, Metric_ID, Metric_Name,
                    [Leading/Lagging], UOM, Year, Performance_Target,
                    Q3_Performance, Q3_Insights, Q4_Performance, Q4_Insights
                    from dbo.tbl_screen_3_V2 where Year = {}
                    '''  
    elif current_quarter == 'Q2':
        year = date.today().year
        query = '''Select Segment, Category, Metric_ID, Metric_Name,
                    [Leading/Lagging], UOM, Year, Performance_Target,
                    Previous_Year_Q4_Performance, Previous_Year_Q4_Insights, 
                    Q1_Performance, Q1_Insights
                    from dbo.tbl_screen_3_V2 where Year = {}
                    ''' 
    elif current_quarter == 'Q3':
        year = date.today().year
        query = '''Select Segment, Category, Metric_ID, Metric_Name,
                    [Leading/Lagging], UOM, Year, Performance_Target,
                    Q1_Performance, Q1_Insights, Q2_Performance, Q2_Insights
                    from dbo.tbl_screen_3_V2 where Year = {}
                    ''' 
    else:
        year = date.today().year
        query = '''Select Segment, Category, Metric_ID, Metric_Name,
                    [Leading/Lagging], UOM, Year, Performance_Target,
                    Q2_Performance, Q2_Insights, Q3_Performance, Q3_Insights
                    from dbo.tbl_screen_3_V2 where Year = {}
                    '''                    
    print(query.format(year))
    #Fetching the data and storing in a dataframe
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    data_table = pd.read_sql_query( query.format(year),cnxn)
    cnxn.close()
    #Fetch the user's accesses
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    access_query = "Select distinct User_persona_name from dbo.tbl_user_personas_V2 where user_id = {}"
    cursor = cnxn.cursor()
    cursor.execute(access_query.format(userid))
    #Filtering the dataframe based on the user's accesses
    personas = []
    for row in cursor:
        personas.append(row[0])
    print(personas)
    if 'Corporate Admin' in personas:
        return data_table
    if 'Corporate System ADMIN' in personas:
        return data_table
    if len(personas) == 2:
        if 'Corporate Reporting Team' or 'Target Definition' in personas:
            return data_table
    if len(personas) == 1:
        if 'View-Only' in personas:
            return data_table
    if 'Segment Insights' or 'Segment Data' or 'Corporate Insights' or 'Corporate Data' in personas:
        filtered_data = pd.DataFrame(columns = list(data_table.columns))
        metric_access_query = "Select * from dbo.tbl_user_inputs_role_V2 where User_id = {}"
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
        access_table = pd.read_sql_query(metric_access_query.format(userid),cnxn)
        cnxn.close()
        for persona in ['Segment Insights', 'Segment Data']:
            segments = list(access_table['Segment_name'][access_table['User_persona_name'] == persona])
            for segment in segments:
                metrics = list(access_table['Metric_id'][(access_table['User_persona_name'] == persona) & (access_table['Segment_name'] == segment)])
                print(persona,segment, metrics)
                df = data_table[(data_table['Segment'] == segment)]
                df = df[df['Metric_ID'].isin(metrics)]
                filtered_data = filtered_data.append(df)
        for persona in['Corporate Insights', 'Corporate Data']:
            metrics = list(access_table['Metric_id'][(access_table['User_persona_name'] == persona) & (access_table['Segment_name'] == 'Corporate')])
            print(persona,segment, metrics)
            #df = data_table[(data_table['Segment'] == 'Corporate')]
            df = data_table[data_table['Metric_ID'].isin(metrics)]
            filtered_data = filtered_data.append(df)
        filtered_data.drop_duplicates(subset = ["Metric_ID", "Segment"],
                     keep = 'first', inplace = True)
        print(filtered_data.to_string())
        return filtered_data

            
def edit_performance(variables, firstresult):
    column_names = list(variables.keys())
    #get old value of metric
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    old_value_query = pd.read_sql_query(("SELECT * FROM dbo.tbl_screen_3_V2 WHERE Metric_ID = " + str(variables['Metric_ID']) + " and Segment = "+ repr(variables['Segment'])), cnxn)
    cnxn.commit()
    cnxn.close() # Close Connection
    #convert old value and new value to json and add to history table
    df = pd.DataFrame(old_value_query, columns= column_names) 
    js = df.to_json(orient = 'records') #convert to json
    new_js = json.dumps(variables)
    #upate history table
    query = 'Select distinct user_name from dbo.user_ref where [ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.edit_performance_history(old_value, new_value, when_updated, updated_by) values (?, ?, ?, ?)", js, new_js, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    #generating the update query
    query = "UPDATE dbo.tbl_screen_3_V2 SET "
    for i in column_names:
        if i != 'Metric_ID':
            x = '[' + i + ']' + ' = ' + ' ?, '
            query = query + x 
    query = query + 'is_disabled = 0 ' + f"WHERE Metric_ID = {variables['Metric_ID']} AND Segment = " + repr(variables['Segment'])
    print(query)
    params = []
    for i in range(0, len(column_names)):
        if column_names[i] != 'Metric_ID':
            params.append(variables[column_names[i]])
    print(params)
    #updating table with new values  
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute(query, params)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    return "Values Updated Successfully!!!"
