import pyodbc
import pandas as pd
import json
from datetime import date
from collections import OrderedDict
import numpy as np

# server = 'mars-horizon.database.windows.net'
# database = 'marsanalyticsdevsqlsrv'
# username = 'mthco'
# password = 'Mathco_123'
server = 'marsanalyticsdevsqlsrv.database.windows.net'
database = 'qmpdevsqldb'
username = 'BI_User'
password = 'B}Gvd,9@pHW%5(b6'

def is_nan(x):
    return (x != x)

#API for detailed report
def displaydata_detailedreport(yr, qt, category):
    #conn through pyodbc execute through pandas
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query(f"""SELECT * FROM dbo.tbl_screen_3_V2 where [Year] = {yr} and [Product/Service] = {repr(category)} and is_disabled = 0""",cnxn)
    cnxn.close()
    #getting column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute("Select top 0 * from dbo.tbl_screen_3_V2")
    colnames = [desc[0] for desc in cursor.description]
    cnxn.close()
    #Creating a list of required columns
    cols_to_keep = [colnames[i] for i in range(0,9)]
    current_qtr = [colnames[i] for i in range(9,len(colnames)) if qt in colnames[i]]
    cols_to_keep = cols_to_keep + current_qtr
    #converting to df
    df = pd.DataFrame(sql_query, columns= cols_to_keep)
    #print(df.to_string())
    #Generating a list of column names in format <segment>_performance, <segment>_insights
    segments = list(df.Segment.unique())
    report_cols = []
    for i in segments:
        a = i + '_' + 'Previous_Year_Q4_Performance'
        b = i + '_' + 'Performance_Target'
        y = i + '_' + current_qtr[0]
        z = i + '_' + current_qtr[1]
        report_cols.append(a)
        report_cols.append(b)
        report_cols.append(y)
        report_cols.append(z)       
    #Creating another df similar to report layout
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query_2 = pd.read_sql_query(f"""SELECT ID, Category, [Metric Name], Year, [Leading/Lagging], UOM FROM dbo.metrics_list_final where [Year] = {yr} and [Product/Service] = {repr(category)} and Is_disabled = 0""",cnxn)
    cnxn.close()
    #getting column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute("Select top 0 ID, Category, [Metric Name], Year, [Leading/Lagging], UOM from dbo.metrics_list_final")
    cols = [desc[0] for desc in cursor.description]
    cnxn.close()
    #converting to df
    df2 = pd.DataFrame(sql_query_2, columns= cols)
    #adding report_cols values as empty columns to df2
    report_cols = cols + report_cols
    df2 = df2.reindex(columns = report_cols)
    #Populating df2 with the performance and insights values from df
    metric_ids = df.Metric_ID.unique()
    #Populating Performance Targets:    
    for i in segments:
        for j in metric_ids:
            value = df[(df['Segment'] == i) & (df['Metric_ID'] == j)]['Performance_Target'].values
            if len(value) != 0 :
                value = value[0]
            else:
                value = np.nan
            column = [col for col in report_cols if i in col and 'Performance_Target' in col ]
            df2.loc[df2['ID'] == j, column] = value
            #print(i,j, value)
    #Populating Previous Year Performance Values:
    for i in segments:
        for j in metric_ids:
            value = df[(df['Segment'] == i) & (df['Metric_ID'] == j)]['Previous_Year_Q4_Performance'].values
            if len(value) != 0 :
                value = value[0]
            else:
                value = np.nan
            column = [col for col in report_cols if i in col and 'Previous_Year_Q4_Performance' in col ]
            df2.loc[df2['ID'] == j, column] = value
            #print(i,j, value)
    #Populating Performance Values:
    for i in segments:
        for j in metric_ids:
            value1 = df[(df['Segment'] == i) & (df['Metric_ID'] == j)][current_qtr[0]].values
            if len(value1) != 0 :
                value1 = value1[0]
            else:
                value1 = np.nan
            value2 = df[(df['Segment'] == i) & (df['Metric_ID'] == j)]['Performance_Target'].values
            if len(value2) != 0 :
                value2 = value2[0]
            else:
                value2 = np.nan
            #print(value1, value2, i, j)
            column = [col for col in report_cols if i in col and current_qtr[0] in col ]
            if not is_nan(value1):
                value = str(value1) + ';' + str(value2)
            else:
                value = value1
            df2.loc[df2['ID'] == j, column] = value
            #print(i,j, value)
    #Populating Performance Insights:
    for i in segments:
        for j in metric_ids:
            value = df[(df['Segment'] == i) & (df['Metric_ID'] == j)][current_qtr[1]].values
            if len(value) != 0 :
                value = value[0]
            else:
                value = np.nan
            column = [col for col in report_cols if i in col and current_qtr[1] in col ]
            df2.loc[df2['ID'] == j, column] = value
    js = df2.to_json(orient = 'records')
    #print(df2.to_string())
    return js
