from re import split
from numpy import product
import pandas as pd
from pandas.io import json
import pyodbc
import datetime
from datetime import date
from flask import jsonify
from collections import OrderedDict


# server = 'mars-horizon.database.windows.net'
# database = 'marsanalyticsdevsqlsrv'
# username = 'mthco'
# password = 'Mathco_123'
server = 'marsanalyticsdevsqlsrv.database.windows.net'
database = 'qmpdevsqldb'
username = 'BI_User'
password = 'B}Gvd,9@pHW%5(b6'


def status_object(result = {}, status = "Success", message = "Completed Successfully"):
    return jsonify(status = status, status_code = 200, message = message, data = result)

def displaydatafunction(): 
    #conn through pyodbc execute through pandas
    current_date = date.today()
    current_year = current_date.year
    query = "SELECT * FROM dbo.metrics_list_final WHERE Is_disabled = 0 AND [Product/Service] = 'Product' AND Year = " + repr(current_year) + " ORDER BY Category"
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query(query,cnxn)
    cnxn.close()
    df_columns = list(sql_query.columns)
    #filter segments based on product/service
    query_2 = "Select Segment from dbo.segment_ref where [Product/Service] = 'Product' and [Is Disabled] = 0 and Year = " + repr(current_year) 
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query_2)
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_to_keep = [item for sublist in column_name for item in sublist]
    #getting the Metric details columns - ID, Name, Category, Year, UOM, Lead/Lag
    columns = df_columns[0:6] 
    segment_columns = []
    for i in df_columns:
        for j in columns_to_keep:
            #print(i,j)
            x = i.split(' ')
            y = x[:-1]
            y = ' '.join(y)
            print(y)
            if j == y:
                segment_columns.append(i)
    columns = columns + segment_columns
    print(columns)
    #converting to df
    df = pd.DataFrame(sql_query, columns= columns)
    #convert to json
    js = df.to_json(orient = 'records')
    return js

def addnewmetric(category,metric_name,lead_lag,uom,year, product_service, comment, firstresult):
    is_disabled = 0
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute("insert into dbo.metrics_list_final(Category, [Metric Name], Year, [Leading/Lagging], UOM, [Is_disabled], [Product/Service]) values (?, ?, ?, ?, ?, ?, ?)",
        category, metric_name, year, lead_lag, uom, is_disabled, product_service)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    #update other tables
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    sqlExecSP = "exec dbo.[UPDATE_METRICS_ADD] @CATEGORY = ?, @METRICNAME = ?, @YEAR = ?, @PRODUCT_SERVICE = ?"
    params = (category, metric_name, year, product_service)
    cursor.execute(sqlExecSP, params)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    #Fetching newly added metric:
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    query = "select * from dbo.metrics_list_final where "
    x = "[Category] = " + repr(category) + " and [Metric Name] = " + repr(metric_name) + " and [Year] = " + repr(year) + " and [Leading/Lagging] =" + repr(lead_lag) + " and [UOM] = " + repr(uom) + " and [Product/Service] = " + repr(product_service)
    query = query + x
    print(query)
    sql_query = pd.read_sql_query(query,cnxn)
    cnxn.close()
    #getting all column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute("Select top 0 * from dbo.metrics_list_final")
    colnames = [desc[0] for desc in cursor.description]
    cnxn.close()
    #getting column names based on product/service
    query_2 = "Select Segment from dbo.segment_ref where [Is Disabled] = 0 and Year = " + repr(year) + "and [Product/Service] = " + repr(product_service)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query_2)
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_df = [item for sublist in column_name for item in sublist]
    cnxn.close()
    #filtering out columns based on product/service 
    segment_columns = []
    for i in colnames:
        for j in columns_df:
            if j in i:
                segment_columns.append(i)
    columns = colnames[0:6]
    columns = columns + segment_columns
    #converting to df
    df = pd.DataFrame(sql_query, columns= columns)
    #df.drop(columns= ['Is_disabled'], inplace=True)
    #convert to json
    js = df.to_json(orient = 'records')
    #Updating cdc table
    query = 'Select distinct user_name from dbo.user_ref where [ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.cdc_table (action,details, [reason for Change], when_updated, updated_by) values ('Metric added',?, ?, ?, ?)", metric_name, comment, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    return js
    
def addnewsegment(segment, comment, product_service,firstresult):
    current_date = date.today()
    current_year = current_date.year
    #Inserting new segment to segment ref table
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute("insert into dbo.segment_ref(Segment, [Add Segment Comment], [Is Disabled], [Year], [Product/Service]) values (?, ?, ?, ?, ?)",segment,comment,0,current_year,product_service)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    #Running SP
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    sqlExecSP = "EXEC [dbo].[ADD_SEGMENT]"
    cursor.execute(sqlExecSP)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    #Updating cdc table
    query = 'Select distinct user_name from dbo.user_ref where [ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.cdc_table (action,details, [reason for Change], when_updated, updated_by) values ('Segment added',?, ?, ?, ?)", segment, comment, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    return "New Segment Added Successfully!!!"

def updatedatafunction(metric_id, comment, firstresult):         
    if len(metric_id) == 1:
        metric_id.append(metric_id[0])
    metric_id = tuple(metric_id)
    #conn through pyodbc execute through pandas
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    query = "UPDATE dbo.metrics_list_final SET Is_disabled = 1 WHERE ID in " + repr(metric_id)
    print(query)
    cursor.execute(query)
    cnxn.commit()
    #update other tables
    for i in metric_id:
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
        cursor = cnxn.cursor()
        sqlExecSP = "exec dbo.[UPDATE_METRICS_DELETE] @METRICID = ?"
        params = (i)
        cursor.execute(sqlExecSP, params)
        cnxn.commit()
        cursor.close() # Close and delete cursor
        del cursor
        cnxn.close() # Close Connection
     #Updating cdc table
    query1 = 'Select distinct user_name from dbo.user_ref where [ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query1)
    result = cursor.fetchall()
    user = result[0][0]
    query2 = 'Select distinct [Metric Name] from dbo.metrics_list_final where [ID] in ' + repr(metric_id)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query2)
    result = cursor.fetchall()
    detail = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.cdc_table (action,details, [reason for Change], when_updated, updated_by) values ('Metric deleted',?, ?, ?, ?)", detail, comment, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    return "The specified record has been deleted"

def delete_segment(segment, comment, firstresult):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    sqlExecSP = "EXEC dbo.DELETE_SEGMENT_1 @COLUMN=?, @COMMENT=?"
    params = (segment, comment)
    cursor.execute(sqlExecSP, params)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    #Updating cdc table
    query = 'Select distinct user_name from dbo.user_ref where [ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.cdc_table (action,details, [reason for Change], when_updated, updated_by) values ('Segment deleted',?, ?, ?, ?)", segment, comment, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    return "Segment Deleted Successfully!!!"

def edit_targets(variables, firstresult):
    column_names = list(variables.keys())
    #get old value of metric
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    old_value_query = pd.read_sql_query(("SELECT * FROM dbo.metrics_list_final WHERE ID = " + str(variables['ID'])), cnxn)
    cnxn.commit()
    cnxn.close() # Close Connection
    #convert old value and new value to json and add to history table
    df = pd.DataFrame(old_value_query, columns= column_names) 
    js = df.to_json(orient = 'records') #convert to json
    new_js = json.dumps(variables)
    #upate history table
    query = 'Select distinct user_name from dbo.user_ref where [ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    # user = cursor.execute(f"select distinct Username from dbo.tbl_users where [User ID] = {firstresult}")
    when = datetime.datetime.utcnow()
    # print(user)
    # print(when)
    cursor.execute("insert into dbo.edit_target_history(old_value, new_value, when_updated, updated_by) values (?, ?, ?, ?, ?)", js, new_js, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    #generating the update query
    query = "UPDATE dbo.metrics_list_final SET "
    for i in column_names:
        if i != 'ID':
            x = '[' + i + ']' + ' = ' + ' ?, '
            query = query + x 
    query = query + 'Is_disabled = 0 ' + f"WHERE ID = {variables['ID']}"
    params = []
    for i in range(1, len(column_names)):
        params.append(variables[column_names[i]])
    #updating table with new values
    print(query)
    print(params)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute(query, params)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    status = get_segments(variables)
    return "Metric Updated Successfully!!!"


def get_segments(json):
    column_names = json.keys()
    target_columns = [col for col in column_names if 'CYT' in col]
    segments_list = []
    for col in target_columns:
        segment = col.replace(' CYT', '')
        segments_list.append(segment)
    #generating list for each segment 
    rows_to_update = []
    for segment in segments_list:
        segment_target = segment + ' CYT'
        segment_target_value = json[segment_target]
        values = []
        values.append(segment_target_value)
        values.append(segment)
        values.append(json['Category'])
        values.append(str(json['ID']))
        values.append(json['Metric Name'])
        values.append(str(json['Year']))
        rows_to_update.append(tuple(values))
    print(rows_to_update)
    query = '''Update dbo.tbl_screen_3_V2 set Performance_Target = ? where 
                Segment = ? and Category = ? and Metric_ID = ? and
                Metric_Name = ? and Year = ?'''        
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.executemany(query, rows_to_update)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    return "Complete"

def copy_metrics(firstresult,comment):
    #create a copy of current year metrics and set year = next year
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    sqlExecSP = "EXEC dbo.COPY_METRICS"
    cursor.execute(sqlExecSP)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    #get list of segments
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute('select [Segment] from dbo.segment_ref where [Is Disabled] = 0')
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
        columns_df = [item for sublist in column_name for item in sublist]
    #print(columns_df)
    cnxn.commit()
    cnxn.close()
    #Set current year's CYT as next year's PYT
    for i in columns_df:
        x = ' CYT' ; y = ' PYT'
        col1 = i+x ; col2 = i+y
        query1 = 'UPDATE dbo.metrics_list_final SET '
        query2 = '[' + col1 + ']' + ' = ' + '[' + col2 + ']'
        query = query1 + query2 + " WHERE [Year] = (YEAR(GETDATE())+1)"
        #print(query)
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
        cursor = cnxn.cursor()
        cursor.execute(query)
        cnxn.commit()
        cnxn.close()
    #Insert the new year into filter data table
    query = 'INSERT INTO dbo.filter_data(Year) VALUES ((YEAR(GETDATE())+1))' 
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute(query)
    cnxn.commit()
    cnxn.close()
    #Updating cdc table
    query = 'Select distinct user_name from dbo.user_ref where [ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.cdc_table (action,details, [reason for Change], when_updated, updated_by) values ('Copy metrics','All metrics copied to next year', ?, ?, ?)", comment, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    return "Metrics copied Successfully!!!"

def yearfilter():
    query = "select distinct Year from dbo.tbl_metrics_history union select distinct Year from dbo.metrics_list_final"
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    year_values = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        year_values.append(row_to_list)
    year_values = [item for sublist in year_values for item in sublist]
    js = json.dumps(year_values)
    # print(js)
    return js

def filtervalues(year, category):
    current_date = date.today()
    current_year = current_date.year
    #Generate queries
    if int(year) < current_year:
        query = "Select * from dbo.tbl_metrics_history where Year = " + f"{year}" + " and [Product/Service] = " + repr(category)
        columns_query = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='tbl_metrics_history'"
    else:
        query = "Select * from dbo.metrics_list_final where [Is_disabled] = 0 and Year = " + f"{year}" + " and [Product/Service] = " + repr(category)
        columns_query = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='metrics_list_final'"
    #Get data
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query(query,cnxn)
    cnxn.close()
    #Get column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(columns_query)
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_df = [item for sublist in column_name for item in sublist]
    cnxn.close()
    #converting to df
    df = pd.DataFrame(sql_query, columns= columns_df)
    #if category:
        #df = df.loc[df['Category'] == category]
    return df

def filterdf(df, year, category):
    #get all of the segments in year specified
    query = "Select Segment from dbo.segment_ref where [Is Disabled] = 0 and Year = " + f"{year}" + "and [Product/Service] = " + repr(category)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    segment_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        segment_name.append(row_to_list)
    cnxn.close()
    segments = [item for sublist in segment_name for item in sublist]
    df_columns = df.columns.values.tolist()
    cols_to_keep = []
    new_cols_list = []
    for i in segments:
        cols_to_keep.append([s for s in df_columns if i in s])
    for i in range(0,6):
        new_cols_list.append(df_columns[i])
    new_cols_list = new_cols_list + [item for sublist in cols_to_keep for item in sublist]
    #remove columns that are not needed from the df
    df2 = df[new_cols_list]
    #convert to json
    js = df2.to_json(orient = 'records')
    return js


def category():
    query1 = "select distinct category from dbo.metrics_list_final where [Product/Service] = 'Product' and Is_Disabled = 0"
    query2 = "select distinct category from dbo.metrics_list_final where [Product/Service] = 'Service' and Is_Disabled = 0"
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query1)
    product = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        product.append(row_to_list)
    cursor.execute(query2)
    service = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        service.append(row_to_list)
    cnxn.close()
    p = [item for sublist in product for item in sublist]
    s = [item for sublist in service for item in sublist]
    # print(p)
    # print(s)
    dict = {'Product': p, 'Service': s}
    # print(dict)
    js = json.dumps(dict)
    # print(js)
    return js

        
